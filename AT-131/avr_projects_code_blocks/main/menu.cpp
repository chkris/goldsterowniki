#include "menu.h"


TMenu::TMenu()
{
    tLcd.init();
    tLcd.defCel();
    blink = 0;
    actualSelectedLang = 0;
    actualPos = 0;
    #if TEMP_CWU
    actualPos = 1;
    # endif
    readEeprom = 0;
    ustSerwisowe = 0;
    save = 1; // czyli jak w��czy napi�cie to nie zapisuje niepotrzebnie do eepromu
    ustRazpalanie = 0;
    buzerOffAlram = 0;
    menuPok = 0;



}

void TMenu::writeEepromMinSek(uint8_t* adress, uint8_t* adress1,  unsigned char min, unsigned char max,unsigned char min1, unsigned char max1){

switch(menuFunc.key){
    case KEY_DOWN:
        if(readEeprom == 1) {
            if(tmpEeprom > min) { tmpEeprom--; }
        }
        if(readEeprom == 2) {
            if(tmpEeprom1 > min1) { tmpEeprom1--; }
        }
        blink = 0;
        menuFunc.key = 0;
    break;
    case KEY_UP:
        if(readEeprom == 1) {
            if(tmpEeprom < max) { tmpEeprom++; }
        }
        if(readEeprom == 2) {
            if(tmpEeprom1 < max1) { tmpEeprom1++; }
        }
        blink = 0;
        menuFunc.key = 0;

    break;

    case KEY_OPTION:
        switch(readEeprom) {
            case 0:
                tmpEeprom = eeprom_read_byte(adress);
                tmpEeprom1 = eeprom_read_byte(adress1);
                minSek = 0;
                readEeprom = 1;
            break;
            case 1:
                minSek = 1;
                readEeprom = 2;
            break;
            case 2:
                if(tmpEeprom != eeprom_read_byte(adress)) {
                    eeprom_write_byte(adress,tmpEeprom);
                }
                if(tmpEeprom1 != eeprom_read_byte(adress1)) {
                    eeprom_write_byte(adress1,tmpEeprom1);
                }
                lookCancel();
                readEeprom = 0;
            break;
        };
        blink = 0;
        menuFunc.key = 0;
    break;

};
}


void TMenu::writeEeprom(uint8_t* adress,  unsigned char min, unsigned char max){

switch(menuFunc.key){
    case KEY_DOWN:
        if(tmpEeprom > min) { tmpEeprom--; }
        blink = 0;
        menuFunc.key = 0;
    break;
    case KEY_UP:
        if(tmpEeprom < max) { tmpEeprom++; }
        blink = 0;
        menuFunc.key = 0;

    break;

    case KEY_OPTION:
        switch(readEeprom) {
            case 1:
                if(tmpEeprom != eeprom_read_byte(adress)) {
                    eeprom_write_byte(adress,tmpEeprom);
                }
                lookCancel();
                readEeprom = 0;
            break;
            case 0:
                tmpEeprom = eeprom_read_byte(adress);
                readEeprom = 1;
            break;
        };
        blink = 0;
        menuFunc.key = 0;
    break;

};
}

void TMenu::displayValueMinSek(char* firstLine,unsigned char value, unsigned char value1, unsigned char freuquency, char minSek)
{
    tLcd.goTo(0,0);
    tLcd.writeFlash(firstLine); tLcd.writeText(" ");
    tLcd.goTo(0,1); // przeniesienie do drugiej linii
    tLcd.writeText(" ");
    switch(minSek){
    case 0:
            //mruganie liczby
            if(blink >= 0 && blink < freuquency){
                if(value < 10) {
                    tLcd.writeNumber(0);
                }
                tLcd.writeNumber(value);
            }
            if(blink >= freuquency && blink < freuquency + 3){
                tLcd.writeText("  ");
            }
            if(blink == freuquency + 3){
                blink = 0;
                tLcd.writeText("  ");
            }
            blink++;
            //mruganie liczby
            tLcd.writeText(" min ");
            if(value1 < 10) {
                tLcd.writeNumber(0);
            }
            tLcd.writeNumber(value1);
            tLcd.writeText(" sek ");
            tLcd.writeText("        ");
    break;
    case 1:
            if(value < 10) {
                tLcd.writeNumber(0);
            }
            tLcd.writeNumber(value);
            tLcd.writeText(" min ");
            //mruganie liczby
            if(blink >= 0 && blink < freuquency){
                if(value1 < 10) {
                    tLcd.writeNumber(0);
                }
                tLcd.writeNumber(value1);
            }
            if(blink >= freuquency && blink < freuquency + 3){
                tLcd.writeText("  ");
            }
            if(blink == freuquency + 3){
                blink = 0;
                tLcd.writeText("  ");

            }
            blink++;
            //mruganie liczby
            tLcd.writeText(" sek ");
            tLcd.writeText("        ");
    break;
    };
}

void TMenu::displayValue(char* firstLine,unsigned char value, unsigned char freuquency, char* unit)
{
    tLcd.goTo(0,0);
    tLcd.writeFlash(firstLine);tLcd.writeText("        ");
    tLcd.goTo(0,1);
    tLcd.writeText("      ");
        //mruganie liczby
            if(blink >= 0 && blink < freuquency){
                tLcd.writeNumber(value);
            }
            if(blink >= freuquency && blink < freuquency + 3){
                tLcd.writeText(" ");
                if(value >= 10) {tLcd.writeText(" ");}
            }
            if(blink == freuquency + 3){
                blink = 0;
                tLcd.writeText(" ");
                if(value >= 10) {tLcd.writeText(" ");}
            }
            blink++;
        //mruganie liczby
            if(unit[0] == 'C') {
                tLcd.writeData(0);
            } else {
                tLcd.writeText(" ");
            }
            tLcd.writeText(unit);
            tLcd.writeText("        ");
}

void TMenu::displayYesOrNo(char* firstLine,int value, unsigned char roz, unsigned char freuquency)
{
        tLcd.goTo(0,0);
        tLcd.writeFlash(firstLine);tLcd.writeText("        ");
        tLcd.goTo(0,1);
        tLcd.writeText("");
        if(roz == 0) {
        switch(value) {
            case 1:
                    if(freuquency > 0) {
                        if(blink >= 0 && blink < freuquency){
                            tLcd.writeFlash((char*)pgm_read_word(&tLangItem.tak[actualSelectedLang]));
                        }
                        if(blink >= freuquency && blink < freuquency + 3){
                            tLcd.writeText("   ");
                        }
                        if(blink == freuquency + 3){
                            blink = 0;
                            tLcd.writeText("   ");
                        }
                        blink++;
                    }
                    tLcd.writeText("   ");
                    tLcd.writeFlash((char*)pgm_read_word(&tLangItem.nie[actualSelectedLang]));
                 break;
            case 0:
            tLcd.writeFlash((char*)pgm_read_word(&tLangItem.tak[actualSelectedLang]));
            tLcd.writeText("   ");
                    if(freuquency > 0) {
                        if(blink >= 0 && blink < freuquency){
                            tLcd.writeFlash((char*)pgm_read_word(&tLangItem.nie[actualSelectedLang]));
                        }
                        if(blink >= freuquency && blink < freuquency + 3){
                            tLcd.writeText("   ");
                        }
                        if(blink == freuquency + 3){
                            blink = 0;
                            tLcd.writeText("   ");
                        }
                        blink++;
                    }
            break;
        };
        }
        if(roz == 1) {
            tLcd.writeText("gotowe");
        }
        tLcd.writeText("          ");

}

void TMenu::dispalyMenu(char* firstLine,char* secondLine, char selected, char freuquency)
{

    if(selected == 0) {
        tLcd.goTo(0,0);
        tLcd.writeData(126);
            //mruganie liczby
            if(blink >= 0 && blink < freuquency){
                tLcd.writeFlash(firstLine);
            }
            if(blink >= freuquency && blink < freuquency + 5){
                tLcd.writeText("               ");
            }
            if(blink == 2*freuquency){
                blink = 0;
            }
            blink++;
        //mruganie liczby
        tLcd.goTo(0,1);
        tLcd.writeText(" ");
        tLcd.writeFlash(secondLine);
    }
    if(selected == 1) {
        tLcd.goTo(0,0);
        tLcd.writeText(" ");
        tLcd.writeFlash(firstLine);

        tLcd.goTo(0,1);
        tLcd.writeData(126);
            //mruganie liczby
            if(blink >= 0 && blink < freuquency){
                tLcd.writeFlash(secondLine);
            }
            if(blink >= freuquency && blink < freuquency + 5){
                tLcd.writeText("               ");
            }
            if(blink == 2*freuquency){
                blink = 0;
            }
            blink++;
        //mruganie liczby
    }

}

void TMenu::displayCoCwu(unsigned char coOrCwu, unsigned int actualTemp, unsigned char setTemp,unsigned char freuquency,unsigned char pok)
{
        tLcd.goTo(0,0);
        switch(coOrCwu) {
            case SET_TEMP_CO:
                 tLcd.writeFlash((char*)pgm_read_word(&tLangItem.zadCo[actualSelectedLang]));
                 break;
            case SET_TEMP_CWU:
                 tLcd.writeFlash((char*)pgm_read_word(&tLangItem.zadCwu[actualSelectedLang]));
                 break;
        };
        tLcd.writeData(1);
        tLcd.writeData(2);
        switch(coOrCwu) {
            case SET_TEMP_CO:
                 tLcd.writeFlash((char*)pgm_read_word(&tLangItem.aktCo[actualSelectedLang]));
                 break;
            case SET_TEMP_CWU:
                 tLcd.writeFlash((char*)pgm_read_word(&tLangItem.aktCwu[actualSelectedLang]));
                 break;
        };
        tLcd.goTo(0,1);
        tLcd.writeText(" ");
        if(freuquency == 0) {
            tLcd.writeNumber(setTemp);
        }
        if(freuquency > 0) {
            if(blink >= 0 && blink < freuquency){
                tLcd.writeNumber(setTemp);
            }
            if(blink >= freuquency && blink < freuquency + 3){
                tLcd.writeText(" ");
                if(setTemp >= 10) {tLcd.writeText(" ");}
            }
            if(blink == freuquency + 3){
                blink = 0;
                tLcd.writeText(" ");
                if(setTemp >= 10) {tLcd.writeText(" ");}
            }
            blink++;
        }

        tLcd.writeData(0);
        tLcd.writeText("C");
        if(pok == 1) {
            tLcd.writeText(" p");
        }
        if(pok == 0){
            tLcd.writeText("  ");
        }
        tLcd.writeData(1);
        tLcd.writeData(2);
        tLcd.writeText("  ");
        tLcd.writeNumber(actualTemp);
        tLcd.writeData(0);
        tLcd.writeText("C ");


}

//funkcje manipuluj�ce po menu//

//szuka najbli�szy index w lang[] o tym samym poziomie//
void TMenu::lookUp(void) {

    unsigned char tmp;

    if(pgm_read_byte(&lang[actualPos].func) == NO_FUNC) {
        tmp = actualPos - 1;

        while ((pgm_read_byte(&lang[tmp].level) != pgm_read_byte(&lang[actualPos].level)) &&
               (pgm_read_byte(&lang[tmp].level) > pgm_read_byte(&lang[actualPos].level)))
        {
            tmp--;
        }

        if(pgm_read_byte(&lang[tmp].level) < pgm_read_byte(&lang[actualPos].level)) {
            actualPos = actualPos;
        } else {
            tMenuScreen.down = actualPos;
            tMenuScreen.up = tmp;
            actualPos = tmp;
        }
        arrow = UP;
    }
    if(pgm_read_byte(&lang[actualPos].func) >= START_FUNC && pgm_read_byte(&lang[actualPos].func) <= END_FUNC){
        menuFunc.key = KEY_UP;
    }
    if(pgm_read_byte(&lang[actualPos].func) == SET_TEMP_CO || pgm_read_byte(&lang[actualPos].func) == SET_TEMP_CWU){
        menuFunc.key = KEY_UP;
    }
}


//szuka najbli�szy index w lang[] o tym samym poziomie//
void TMenu::lookDown(void) {

    unsigned char tmp;

    if(pgm_read_byte(&lang[actualPos].func) == NO_FUNC) {
        tmp = actualPos + 1;
        while ((pgm_read_byte(&lang[tmp].level) != pgm_read_byte(&lang[actualPos].level)) &&
               (pgm_read_byte(&lang[tmp].level) > pgm_read_byte(&lang[actualPos].level)))
        {
            tmp++;
        }

        if(pgm_read_byte(&lang[tmp].level) < pgm_read_byte(&lang[actualPos].level)) {
            actualPos = actualPos;
        } else {
            tMenuScreen.up = actualPos;
            tMenuScreen.down = tmp;
            actualPos = tmp;
        }
        arrow = DOWN;
    }
    if(pgm_read_byte(&lang[actualPos].func) >= START_FUNC && pgm_read_byte(&lang[actualPos].func) <= END_FUNC) {
        menuFunc.key = KEY_DOWN;
    }
    if(pgm_read_byte(&lang[actualPos].func) == SET_TEMP_CO || pgm_read_byte(&lang[actualPos].func) == SET_TEMP_CWU){
        menuFunc.key = KEY_DOWN;
    }
}


//sprawdza czy nast�pny ma wy�szy poziom i zwi�ksza index//
void TMenu::lookOption(void) {

    unsigned char tmp;

    menuFunc.key = KEY_OPTION;
    blink = 0;
    //je�li nast�pna pozycja w menu jest z zakresu funkcji, to zwi�ksz indeks o jeden
    if(pgm_read_byte(&lang[actualPos + 1].func) >= START_FUNC && pgm_read_byte(&lang[actualPos + 1].func) <= END_FUNC) {
        actualPos++;

    }

    if(pgm_read_byte(&lang[actualPos].func) == NO_FUNC) {

        if (pgm_read_byte(&lang[actualPos + 1].level) > pgm_read_byte(&lang[actualPos].level))
        {
            actualPos++;
            tmp = actualPos + 1;
            while ((pgm_read_byte(&lang[tmp].level) != pgm_read_byte(&lang[actualPos].level)) &&
                   (pgm_read_byte(&lang[tmp].level) > pgm_read_byte(&lang[actualPos].level)))
            {
                tmp++;
            }
            tMenuScreen.up = actualPos;
            tMenuScreen.down = tmp;
            arrow = UP;
        }


    }

    if((pgm_read_byte(&lang[actualPos].func) == SET_TEMP_CO || pgm_read_byte(&lang[actualPos].func) == SET_TEMP_CWU) && save == 1) {
#if ONLY_FAN
        actualPos = 1;
        arrow = UP;
        tMenuScreen.up = actualPos;
        tMenuScreen.down = 3;
#endif
#if PODAJNIK
        actualPos = 1;
        arrow = UP;
        tMenuScreen.up = actualPos;
        tMenuScreen.down = 3;
#endif
#if TEMP_CWU
        actualPos = 2;
        arrow = UP;
        tMenuScreen.up = actualPos;
        tMenuScreen.down = 4;
#endif



    }
}


//szuka najbli�szy index w lang[] o tym poziomie o jeden ni�szym//
void TMenu::lookCancel(void) {

    unsigned char tmp;

    menuFunc.key = KEY_CANCEL;
    blink = 0;


        if(pgm_read_byte(&lang[actualPos].func) == SET_TEMP_CO || pgm_read_byte(&lang[actualPos].func) == SET_TEMP_CWU) {

        #if TEMP_CWU
        actualPos = (actualPos + 1) % 2;
        #endif

        tMenuScreen.up = actualPos;

    }

    if(pgm_read_byte(&lang[actualPos].func) == NO_FUNC ||
    (pgm_read_byte(&lang[actualPos].func) >= START_FUNC && pgm_read_byte(&lang[actualPos].func) <= END_FUNC)
    ) {

        tmp = actualPos - 1;
        while(pgm_read_byte(&lang[tmp].level) != (pgm_read_byte(&lang[actualPos].level) - 1))
        {
            tmp--;
        }
        actualPos = tmp;
        tmp++;
        while ((pgm_read_byte(&lang[tmp].level) != pgm_read_byte(&lang[actualPos].level)) &&
               (pgm_read_byte(&lang[tmp].level) > pgm_read_byte(&lang[actualPos].level)))
        {
            tmp++;
        }
        if(pgm_read_byte(&lang[tmp].level) < pgm_read_byte(&lang[actualPos].level))
        {
            tmp = actualPos - 1;
            while ((pgm_read_byte(&lang[tmp].level) != pgm_read_byte(&lang[actualPos].level)) &&
                   (pgm_read_byte(&lang[tmp].level) > pgm_read_byte(&lang[actualPos].level)))
            {
                tmp--;
            }
            tMenuScreen.up = tmp;
            tMenuScreen.down = actualPos;
            arrow = DOWN;
        } else {
            tMenuScreen.up = actualPos;
            tMenuScreen.down = tmp;
            arrow = UP;
        }
    }
    readEeprom = 0;

}





