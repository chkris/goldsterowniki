#ifndef _TEXT_EN_H__INCLUDED_
#define _TEXT_EN_H__INCLUDED_
// najni�szy w ga��zi plik�w

#define PIN_POKOJOWKA PINA
#define NR_PIN_POKOJOWKA PINA7

#define POKOJOWKA           1
#define TEMP_CWU            0
#define PODAJNIK            0
#define ONLY_FAN            !PODAJNIK

//special
const char  en_aktCo[] PROGMEM = " ACT CO";
const char  en_zadCo[] PROGMEM = "SET CO ";
const char  en_aktCwu[] PROGMEM = "ACT CWU";
const char  en_zadCwu[] PROGMEM = "SET CWU";
const char  en_tak[] PROGMEM = "yes";
const char  en_nie[] PROGMEM = "no ";

const char  en_ustNadmuch[]         PROGMEM = "settings fan   ";
const char  en_tempZal[]            PROGMEM = "temp start     ";
const char  en_przedmuchPraca[]     PROGMEM = "work           ";
const char  en_przedmuchPrzerwa[]   PROGMEM = "break          ";
const char  en_histereza[]          PROGMEM = "histerezis     ";
const char  en_silaNadmuchu[]       PROGMEM = "fan power      ";
const char  en_ustPompaCO[]         PROGMEM = "setting pump CO";
const char  en_rozpalanie[]         PROGMEM = "burning        ";
const char  en_ustSerwis[]          PROGMEM = "setting service";

//PODAJNIK
const char en_ustPodajnik[]            PROGMEM = "feeder setting ";
const char en_rozpalaniePraca[]        PROGMEM = "burning work   ";
const char en_rozpalaniePrzerwa[]      PROGMEM = "burning pause  ";
//PODAJNIK

#if TEMP_CWU
const char  en_ustPompaCWU[]        PROGMEM = "setting pump CW";
#endif

#if POKOJOWKA
const char  en_pokojowka[]          PROGMEM = "house controler";
#endif




#endif
