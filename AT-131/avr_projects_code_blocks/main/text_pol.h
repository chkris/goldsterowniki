#ifndef _TEXT_POL_H__INCLUDED_
#define _TEXT_POL_H__INCLUDED_

#include "text_en.h"
//special
const char pol_aktCo[] PROGMEM = " AKT CO";
const char pol_zadCo[] PROGMEM = "ZAD CO ";
const char pol_aktCwu[] PROGMEM = "AKT CWU";
const char pol_zadCwu[] PROGMEM = "ZAD CWU";
const char pol_tak[] PROGMEM = "tak";
const char pol_nie[] PROGMEM = "nie";

const char pol_ustNadmuch[]         PROGMEM = "ust nadmuch    ";
const char pol_tempZal[]            PROGMEM = "temp zal kotla ";
const char pol_tempZalCo[]          PROGMEM = "temp zalaczania";
const char pol_przedmuchPraca[]     PROGMEM = "przedmuch praca";
const char pol_przedmuchPrzerwa[]   PROGMEM = "przedmuch przer";
const char pol_histereza[]          PROGMEM = "histereza      ";
const char pol_histerezaKotla[]          PROGMEM = "histereza kotla";
const char pol_silaNadmuchu[]       PROGMEM = "sila nadmuchu  ";
const char pol_ustPompaCO[]         PROGMEM = "ust pompa CO   ";
const char pol_rozpalanie[]         PROGMEM = "rozpalanie     ";
const char pol_ustSerwis[]          PROGMEM = "ust serwisowe  ";
//PODAJNIK
const char pol_ustPodajnik[]            PROGMEM = "ust podajnik   ";
const char pol_rozpalaniePraca[]        PROGMEM = "czas podawania ";
const char pol_rozpalaniePrzerwa[]      PROGMEM = "przer podawania ";
//PODAJNIK
#if TEMP_CWU
const char pol_ustPompaCWU[]        PROGMEM = "ust pompa CWU  ";
#endif

#if POKOJOWKA
const char pol_pokojowka[]          PROGMEM = "pokojowka      ";
#endif

const char pol_pracaReczna[]        PROGMEM = "praca reczna   ";
const char pol_podajnik[]           PROGMEM = "podajnik       ";
const char pol_nadmuch[]            PROGMEM = "nadmuch        ";
const char pol_pompaCo[]            PROGMEM = "pompa CO       ";
const char pol_pompaCwu[]           PROGMEM = "pompa Cwu      ";
//ALARMY
const char pol_ALARMCO[]            PROGMEM = "ALARM: temp CO ";
const char pol_ALARMPOD[]           PROGMEM = "ALARM: temp POD";
const char pol_ALARMCOU[]           PROGMEM = "ALARM: uszk CO  ";
const char pol_ALARMPODU[]          PROGMEM = "ALARM: uszk POD ";

const char pol_CLEAR[]              PROGMEM = "                ";
#endif

