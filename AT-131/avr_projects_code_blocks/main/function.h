/*
 *      function.cpp
 *      avrlib
 *      Created by Krzysztof Cholewka on April 2009
 *      Copyright (c). All rights reserved.
 */

/* function is a interface for Tfunction class
 * contained all function of controller
 */

#ifndef _FUNCTION_H__INCLUDED_
#define _FUNCTION_H__INCLUDED_



#include <avr/io.h>
#include <avr/delay.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include "text_pol.h"

#define FEEDER_ON               PORTA |= _BV(1); DDRA |= _BV(1);
#define FEEDER_OFF              PORTA &= ~_BV(1); DDRA &= ~_BV(1);

#define DIODA_FEEDER_ON         PORTB |= _BV(3); DDRB |= _BV(3);
#define DIODA_FEEDER_OFF        PORTB &= ~_BV(3); DDRB &= ~_BV(3);


#define PUMP_CWU_ON             PORTA |= _BV(1); DDRA |= _BV(1);
#define PUMP_CWU_OFF            PORTA &= ~_BV(1); DDRA &= ~_BV(1);

#define DIODA_CWU_ON            PORTB |= _BV(3); DDRB |= _BV(3);
#define DIODA_CWU_OFF           PORTB &= ~_BV(3); DDRB &= ~_BV(3);

#define FAN_ON                  PORTA |= _BV(3); DDRA |= _BV(3);
#define FAN_OFF                 PORTA &= ~_BV(3); DDRA &= ~_BV(3);

#define DIODA_FAN_ON            PORTB |= _BV(5); DDRB |= _BV(5);
#define DIODA_FAN_OFF           PORTB &= ~_BV(5); DDRB &= ~_BV(5);

#define PUMP_CO_ON              PORTA |= _BV(2); DDRA |= _BV(2);
#define PUMP_CO_OFF             PORTA &= ~_BV(2); DDRA &= ~_BV(2);

#define DIODA_PUMP_CO_ON        PORTB |= _BV(4); DDRB |= _BV(4);
#define DIODA_PUMP_CO_OFF       PORTB &= ~_BV(4); DDRB &= ~_BV(4);

#define MOC_ON                  PORTA |= _BV(4); DDRA |= _BV(4)
#define MOC_OFF                 PORTA &= ~_BV(4); DDRA &= ~_BV(4)

#define MAX_TEMP_CO     80

typedef unsigned char u8;

#define NO_ERROR            0
#define BLAD_CZUJNIKA_CO    1
#define ZA_DUZA_TEMP_KOTLA  2
#define BLAD_CZUJNIKA_CWU   3
#define BLAD_CZUJNIKA_POD   4
#define ZA_DUZA_TEMP_POD    5

#define ANTY_STOP_PRZERWA 5
#define ANTY_STOP_PRACA 30

#define ALARM_POD 65 // przy 65 stopniach



struct TTime
{
    unsigned int ms;
    unsigned int s;
    unsigned int m;
    unsigned int h;
    unsigned int d;
};

struct TParameters {
    unsigned char tempZadKotla;
    int tempAktKotla;
    unsigned char tempPumpCoStart;
    unsigned char tempNadmuchStart;
    // do pompy CWU
    unsigned char tempPompaCwuStart;
    unsigned char tempPompaCwuStop;
    int tempAktCwu;
    unsigned char hisPompaCwu;
    // do pompy CWU
    unsigned char hisPompaCo;

    unsigned char hisKotla;// regulator dla pompy Cwu bedzie mia� jedn� histerez� dla wszystkich urz�dze�
//    unsigned char hisPompaCo;
// NADMUCH
    unsigned char przerwaNadPodMin;
    unsigned char pracaNadPodMin;
    unsigned char przerwaNadPodSek;
    unsigned char pracaNadPodSek;
// NADMUCH
// PODAJNIK

    unsigned char przerwaFeederRozMin;
    unsigned char przerwaFeederRozSek;

    unsigned char pracaFeederRozMin;
    unsigned char pracaFeederRozSek;
// PODAJNIK
};


struct TFan {
    char zeroTime;
    char state;
    struct TTime time;
    char zeroState;
};

struct TFeeder {
    char zeroTime;
    char state;
    struct TTime time;
    char zeroState;
};

struct TPumpCo {
    char zeroTime;
    char state;
    struct TTime time;
};

struct TPumpCwu {
    char zeroTime;
    char state;
    struct TTime time;
};

class TFunc
{
    public:
        TFunc();


        unsigned char rozpalanie;
        unsigned char hisFanStart;  //przy za��czaniu nadmuchu histereza, taka sama jak przy osi�gni�tej temperaturze
        unsigned char hisFan;
        unsigned char hisPumpCo;
        unsigned char hisPumpCwu;
        unsigned char hisPumpCwuStart;
        unsigned char ERROR;
        TFeeder feeder;
        TFan fan;
        TPumpCo pumpCo;
        TPumpCwu pumpCwu;
        TTime fanTime, pumpCoTime;
        TParameters param;

        unsigned char pinPok;
        unsigned char menuPok;


        unsigned char rozpalono;

        void mesureTime(struct TTime* time);
        void setTime(u8 d,u8 h, u8 m, u8 s, u8 ms, struct TTime* time);
        void toggle(unsigned char workMin, unsigned char workSek, unsigned char pauseMin, unsigned char pauseSek, struct TTime* time, char* state, char* zeroTime);
        void nadmuch(TParameters param);
        void pompaCo(TParameters param);
        void pompaCwu(TParameters param);
};

#endif
