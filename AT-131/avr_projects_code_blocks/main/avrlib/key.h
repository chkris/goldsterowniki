/*
 *      key.cpp
 *      avrlib
 *      Created by Krzysztof Cholewka on April 2009
 *      Copyright (c). All rights reserved.
 */

/* key is a interface for TKey class
 * contained all helpfull function for
 * detecting press, keep switches.
 */

#ifndef _KEY_H__INCLUDED_
#define _KEY_H__INCLUDED_

#include <avr/io.h>

#define SWITCH_PIN_1 PIND
#define SWITCH_1 PIND2

#define SWITCH_PIN_2 PIND
#define SWITCH_2 PIND3

#define SWITCH_PIN_3 PIND
#define SWITCH_3 PIND4

#define SWITCH_PIN_4 PIND
#define SWITCH_4 PIND5

#define TEST_SWITCH(NR) !(SWITCH_PIN_## NR & _BV(SWITCH_## NR))

#define KEY_NO_PRESS    0
#define KEY_UP          3
#define KEY_DOWN        2
#define KEY_OPTION      4
#define KEY_CANCEL      1

#define DELAY_KEEP 10
#define DELAY_TIME 2

#define BUZER_ON DDRD |= (1 << PD6); PORTD |= (1 << PD6)
#define BUZER_OFF DDRD &= ~(1 << PD6); PORTD |= (1 << PD6)

class TKey
{
    public:
        TKey();
        void init(void);
        void test(void);
        volatile char keyPressed;
        volatile char lastKey;
        volatile int countKey;
        volatile char buzerOff;
        volatile unsigned char delayKey;
};

#endif

