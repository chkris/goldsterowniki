/*
 *      pcf8583.cpp
 *      avrlib
 *      Created by Krzysztof Cholewka on February 2008
 *      Copyright (c). All rights reserved.
 */

/* pcf8583 is a interface for TPcf8583 class
 * contained all function of TPcf8583 clock/calendar
 */

#ifndef _PCF8583_H__INCLUDED_
#define _PCF8583_H__INCLUDED_

#include <avr/io.h>
#include <avr/delay.h>
#include "i2c.h"


#define PCF8583_A0	0

class TPcf8583
{

    public:
        TPcf8583();
        void setTime(u8 day, u8 hour,u8 min,u8 sec);
        void getTime(u8* day, u8 *hour, u8 *min, u8 *sec);


        void holdOn(void);
        void holdOff(void);
        void start(void);
        void stop(void);
        u8 getStatus(void);
        u8 read(u8 address);
        void write(u8 address, u8 data);
        u8 readBcd(u8 address);
        void writeBcd(u8 address,u8 data);
        u8 bcd2bin(u8 bcd);
        u8 bin2bcd(u8 bin);

        TI2c i2c;
        u8 status;
        u8 alarm;

};

#endif
