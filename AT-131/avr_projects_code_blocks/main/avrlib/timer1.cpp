#include "timer1.h"
#include <avr/io.h>

TTimerOne::TTimerOne()
{
}

void TTimerOne::init(void)
{
    TCCR0 = INTP0_CLK1;            //prescaler 64
    TCNT0 = 0;                        //reset counter
    TIMSK = _BV(TOIE0);               // enable TCNT0 overflow interrupt
}
