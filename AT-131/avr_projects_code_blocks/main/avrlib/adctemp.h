/*
 *      adctemp.cpp
 *      avrlib
 *      Created by Krzysztof Cholewka on January 2008
 *      Copyright (c). All rights reserved.
 */

/* adctemp is a interface for TAdcTemp class
 * contained all helpfull function for
 * mesureing temperature by using kty of 2
 * kilos resistance witch analog to digital converter.
 */

#ifndef _ADCTEMP_H__INCLUDED_
#define _ADCTEMP_H__INCLUDED_

#include <avr/io.h>

#define RT25    	    	2000
#define DU_10_BIT 		    0.0048828
#define DU_8_BIT 		    0.019453125
#define ALPHA	    		0.00788
#define BETHA   			0.00001937

//troche bardzieje zoptymalizowane ze sta�ymi
#define A   9.7656
#define B   248.3776
#define C   0.15496
#define D   15.76
#define E   0.07748

#define BUFER_TIME 7
#define BUFER_COUNT 40

struct TMyFloat
{
    int all;
    unsigned char decimal;
};

struct TTempError
{
    float temp;
    int wynik;
};


class TAdcTemp
{
    public:
        TAdcTemp();
        void init(void);
        TTempError temp(int, unsigned char);
        int buferCo;
        int buferCwu;
        volatile unsigned char buferTime;
        unsigned char buferCount;
        int tempCo;
        int tempCwu;
        int wynikCo;
        int wynikCwuPod;//do podajnika i cwu to samo
        //TMyFloat dTTemp(int, unsigned char);

};

#endif
