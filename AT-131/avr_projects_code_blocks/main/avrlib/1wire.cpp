#include "1wire.h"

TOneWire::TOneWire()
{
}

u8 TOneWire::inputPinState()
{
	return OW_GET_IN();
}

void TOneWire::parasiteEnable(void)
{
    OW_OUT_HIGH();
	OW_DIR_OUT();
}

void TOneWire::parasiteDisable(void)
{
    OW_OUT_LOW();
	OW_DIR_IN();
}

u8 TOneWire::reset(void)
{
	u8 err;
	u8 sreg;

	OW_OUT_LOW(); // disable internal pull-up (maybe on from parasite)
	OW_DIR_OUT(); // pull OW-Pin low for 480us
    _delay_loop_2(120);
    _delay_loop_2(120);
    _delay_loop_2(120);
    _delay_loop_2(120);
	//delay_us(480);

	sreg=SREG;
	cli();

	// set Pin as input - wait for clients to pull low
	OW_DIR_IN(); // input

//	delay_us(66);
	_delay_loop_2(16);
	_delay_loop_2(16);
	_delay_loop_2(16);
	_delay_loop_2(16);
	err = OW_GET_IN();		// no presence detect
	// nobody pulled to low, still high

	SREG = sreg; // sei()

	// after a delay the clients should release the line
	// and input-pin gets back to high due to pull-up-resistor
	//delay_us(480-66);
	_delay_loop_2(103);
	_delay_loop_2(103);
	_delay_loop_2(103);
	_delay_loop_2(103);
	if( OW_GET_IN() == 0 )		// short circuit
		err = 1;

	return err;
}

u8 TOneWire::bitIo(u8 b)
{
	u8 sreg;

	sreg = SREG;
	cli();

	OW_DIR_OUT(); // drive bus low

	//delay_us(1); // Recovery-Time wuffwuff was 1
	_delay_loop_1(1);
	_delay_loop_1(1);
	_delay_loop_1(1);
	_delay_loop_1(1);
	if (b) OW_DIR_IN(); // if bit is 1 set bus high (by ext. pull-up)

	// wuffwuff delay was 15uS-1 see comment above
	//delay_us(15-1-OW_CONF_DELAYOFFSET);
    _delay_loop_2(3);
    _delay_loop_2(3);
    _delay_loop_2(3);
    _delay_loop_2(3);

	if( OW_GET_IN() == 0 ) b = 0;  // sample at end of read-timeslot

	//delay_us(60-15);
	_delay_loop_2(11);
	_delay_loop_2(11);
	_delay_loop_2(11);
	_delay_loop_2(11);
	OW_DIR_IN();

	SREG=sreg; // sei();

	return b;
}

u8 TOneWire::byteWrite(u8 b)
{
	u8 i = 8, j;

	do {
		j = bitIo(b & 1);
		b >>= 1;
		if( j ) b |= 0x80;
	} while(--i);

	return b;
}

u8 TOneWire::byteRead(void)
{
  // read by sending 0xff (a dontcare?)
  return byteWrite(0xFF);
}

u8 TOneWire::romSearch(u8 diff, u8 *id)
{
	u8 i, j, next_diff;
	u8 b;

	if(reset() ) return OW_PRESENCE_ERR;	// error, no device found

	byteWrite( OW_SEARCH_ROM );			// ROM search command
	next_diff = OW_LAST_DEVICE;			// unchanged on last device

	i = OW_ROMCODE_SIZE * 8;					// 8 bytes

	do {
		j = 8;					// 8 bits
		do {
			b = bitIo( 1 );			// read bit
			if( bitIo( 1 ) ) {			// read complement bit
				if( b )					// 11
				return OW_DATA_ERR;			// data error
			}
			else {
				if( !b ) {				// 00 = 2 devices
					if( diff > i || ((*id & 1) && diff != i) ) {
					b = 1;				// now 1
					next_diff = i;			// next pass 0
					}
				}
			}
			bitIo( b );     			// write bit
			*id >>= 1;
			if( b ) *id |= 0x80;			// store bit

			i--;

		} while( --j );

		id++;					// next byte

	} while( i );

	return next_diff;				// to continue search
}

void TOneWire::command(u8 command, u8 *id)
{
	u8 i;

	reset();

	if( id ) {
		byteWrite( OW_MATCH_ROM );			// to a single device
		i = OW_ROMCODE_SIZE;
		do {
			byteWrite( *id );
			id++;
		} while( --i );
	}
	else {
		byteWrite( OW_SKIP_ROM );			// to all devices
	}

	byteWrite( command );
}
