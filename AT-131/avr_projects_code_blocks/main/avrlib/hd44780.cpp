#include "hd44780.h"




PROGMEM  char defDegre[8]  = {
    MAKE_LCD_BYTE(0,0,X,X,X),
    MAKE_LCD_BYTE(0,0,X,0,X),
    MAKE_LCD_BYTE(0,0,X,X,X),
    MAKE_LCD_BYTE(0,0,0,0,0),
    MAKE_LCD_BYTE(0,0,0,0,0),
    MAKE_LCD_BYTE(0,0,0,0,0),
    MAKE_LCD_BYTE(0,0,0,0,0),
    MAKE_LCD_BYTE(0,0,0,0,0)
};

PROGMEM  char defSeparatorRight[8]  = {
    MAKE_LCD_BYTE(X,X,0,0,0),
    MAKE_LCD_BYTE(X,X,0,0,0),
    MAKE_LCD_BYTE(X,X,0,0,0),
    MAKE_LCD_BYTE(X,X,0,0,0),
    MAKE_LCD_BYTE(X,X,0,0,0),
    MAKE_LCD_BYTE(X,X,0,0,0),
    MAKE_LCD_BYTE(X,X,0,0,0),
    MAKE_LCD_BYTE(X,X,0,0,0)
};

PROGMEM  char defSeparatorLeft[8]  = {
    MAKE_LCD_BYTE(0,0,0,X,X),
    MAKE_LCD_BYTE(0,0,0,X,X),
    MAKE_LCD_BYTE(0,0,0,X,X),
    MAKE_LCD_BYTE(0,0,0,X,X),
    MAKE_LCD_BYTE(0,0,0,X,X),
    MAKE_LCD_BYTE(0,0,0,X,X),
    MAKE_LCD_BYTE(0,0,0,X,X),
    MAKE_LCD_BYTE(0,0,0,X,X)
};


PROGMEM  char defGOLDleft[8]  = {
    MAKE_LCD_BYTE(X,X,X,0,0),
    MAKE_LCD_BYTE(X,0,0,0,0),
    MAKE_LCD_BYTE(X,0,0,0,0),
    MAKE_LCD_BYTE(0,0,0,0,0),
    MAKE_LCD_BYTE(0,0,0,0,X),
    MAKE_LCD_BYTE(0,0,0,0,X),
    MAKE_LCD_BYTE(0,0,X,X,X),
    MAKE_LCD_BYTE(0,0,0,0,0),
};

TLcd::TLcd()
{
}

void TLcd::outNibble(unsigned char nibbleToWrite)
{
    if(nibbleToWrite & 0x01)
        LCD_DB4_PORT |= LCD_DB4;
    else
        LCD_DB4_PORT  &= ~LCD_DB4;

    if(nibbleToWrite & 0x02)
        LCD_DB5_PORT |= LCD_DB5;
    else
        LCD_DB5_PORT  &= ~LCD_DB5;

    if(nibbleToWrite & 0x04)
        LCD_DB6_PORT |= LCD_DB6;
    else
        LCD_DB6_PORT  &= ~LCD_DB6;

    if(nibbleToWrite & 0x08)
        LCD_DB7_PORT |= LCD_DB7;
    else
        LCD_DB7_PORT  &= ~LCD_DB7;
}

unsigned char TLcd::inNibble(void)
{
    unsigned char tmp = 0;

    if(LCD_DB4_PIN & LCD_DB4)
        tmp |= (1 << 0);
    if(LCD_DB5_PIN & LCD_DB5)
        tmp |= (1 << 1);
    if(LCD_DB6_PIN & LCD_DB6)
        tmp |= (1 << 2);
    if(LCD_DB7_PIN & LCD_DB7)
        tmp |= (1 << 3);
    return tmp;
}

void TLcd::write(unsigned char dataToWrite)
{
    LCD_DB4_DIR |= LCD_DB4;
    LCD_DB5_DIR |= LCD_DB5;
    LCD_DB6_DIR |= LCD_DB6;
    LCD_DB7_DIR |= LCD_DB7;

    LCD_RW_PORT &= ~LCD_RW;
    LCD_E_PORT |= LCD_E;
    outNibble(dataToWrite >> 4);
    LCD_E_PORT &= ~LCD_E;
    LCD_E_PORT |= LCD_E;
    outNibble(dataToWrite);
    LCD_E_PORT &= ~LCD_E;
    for(char i = 0; i < 8; i++){
        _delay_loop_2(220);
    }
    //while(readStatus()&0x80);
}

unsigned char TLcd::read(void)
{
    unsigned char tmp = 0;
    LCD_DB4_DIR &= ~LCD_DB4;
    LCD_DB5_DIR &= ~LCD_DB5;
    LCD_DB6_DIR &= ~LCD_DB6;
    LCD_DB7_DIR &= ~LCD_DB7;

    LCD_RW_PORT |= LCD_RW;
    LCD_E_PORT |= LCD_E;
    tmp |= (inNibble() << 4);
    LCD_E_PORT &= ~LCD_E;
    LCD_E_PORT |= LCD_E;
    tmp |= inNibble();
    LCD_E_PORT &= ~LCD_E;
    return tmp;
}

void TLcd::init(void)
{
    LCD_DB4_DIR |= LCD_DB4; // Konfiguracja kierunku pracy wyprowadzeń
    LCD_DB5_DIR |= LCD_DB5; //
    LCD_DB6_DIR |= LCD_DB6; //
    LCD_DB7_DIR |= LCD_DB7; //
    LCD_E_DIR 	|= LCD_E;   //
    LCD_RS_DIR 	|= LCD_RS;  //
    LCD_RW_DIR 	|= LCD_RW;  //
    for(char i = 0; i < 8; i++){
    _delay_loop_2(15 *250 * MY_F_CPU);//15ms//F_CPU = 1          // oczekiwanie na ustalibizowanie się napiecia zasilajacego
    }
    LCD_RS_PORT &= ~LCD_RS; // wyzerowanie linii RS
    LCD_E_PORT &= ~LCD_E;   // wyzerowanie linii E
    LCD_RW_PORT &= ~LCD_RW;

    for(char i = 0; i < 3; i++){
        LCD_E_PORT |= LCD_E;  //  E = 1
        outNibble(0x03); // tryb 8-bitowy
        LCD_E_PORT &= ~LCD_E; // E = 0
       for(char i = 0; i < 8; i++){
        _delay_loop_2(5 *250 * MY_F_CPU);//5ms//F_CPU = 1
       }
    }

    LCD_E_PORT |= LCD_E;    // E = 1
    outNibble(0x02);   // tryb 4-bitowy
    LCD_E_PORT &= ~LCD_E;   // E = 0
    for(char i = 0; i < 8; i++){
    _delay_loop_2(1 *250 * MY_F_CPU);//1ms//F_CPU = 1           // czekaj 1ms
    }
    writeCommand(HD44780_FUNCTION_SET | HD44780_FONT5x7 | HD44780_TWO_LINE | HD44780_4_BIT); // interfejs 4-bity, 2-linie, znak 5x7
    writeCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_OFF);                               // wyczyszczenie wyswietlacza
    writeCommand(HD44780_CLEAR);                                                             // czyszczenie zawartości pamieci DDRAM
    writeCommand(HD44780_ENTRY_MODE | HD44780_EM_SHIFT_CURSOR | HD44780_EM_INCREMENT);       // inkrementaja adresu i przesuwanie kursora
    writeCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_ON | HD44780_CURSOR_OFF | HD44780_CURSOR_NOBLINK); // włącz LCD, bez kursora i mrugania
}

void TLcd::writeCommand(unsigned char commandToWrite)
{
    LCD_RS_PORT &= ~LCD_RS;
    write(commandToWrite);
}

unsigned char TLcd::readStatus(void)
{
    LCD_RS_PORT &= ~LCD_RS;
    return read();
}

void TLcd::writeData(unsigned char dataToWrite)
{
    LCD_RS_PORT |= LCD_RS;
    write(dataToWrite);
}

unsigned char TLcd::readData(void)
{
    LCD_RS_PORT |= LCD_RS;
    return read();
}

void TLcd::writeText(char *text)
{
    while(*text) writeData(*text++);
}

void TLcd::goTo(unsigned char x, unsigned char y)
{
    writeCommand(HD44780_DDRAM_SET | (x + (0x40 * y)));
}

void TLcd::clear(void)
{
    writeCommand(HD44780_CLEAR);
    _delay_loop_2(250);//2ms//F_CPU = 1
}

void TLcd::home(void)
{
    writeCommand(HD44780_HOME);
    //_delay_loop_2(250 * MY_F_CPU);//2ms//F_CPU = 1
}

void TLcd::writeNumber(int allOutNumber)
{
    if(allOutNumber < 10){
        writeData(allOutNumber % 10 + 48);
    }
    if((allOutNumber >= 10) && (allOutNumber < 100)){
        writeData(allOutNumber / 10 + 48);
        writeData(allOutNumber % 10 + 48);
    }
    if((allOutNumber >= 100) && (allOutNumber < 1000)){
        writeData((allOutNumber / 100) + 48);
        writeData((allOutNumber / 10 - allOutNumber / 100 * 10) + 48);
        writeData((allOutNumber % 10) % 10 + 48);
    }
    if(allOutNumber >= 1000) {
        writeData((allOutNumber / 1000) + 48);
        writeData((allOutNumber / 100 - allOutNumber / 1000 * 10) + 48);
        writeData((allOutNumber / 10 - allOutNumber / 100 * 10) + 48);
        writeData((allOutNumber % 10) % 10 + 48);
    }
}

void TLcd::writeFlash(char* p)
{
    register char c;


    while (c = pgm_read_byte(p++)) {
        writeData(c);
    }


}

void TLcd::defCustomChar(char *s,unsigned char CGRAM_pos)
{
  unsigned char l;
  writeCommand(CGRAM_pos);		                        //ustaw adres CGRAM na 0
  for(l=0;l<8;l++) writeData(pgm_read_byte(s++));	//wpisuj kolejne definicje do CGRAM
  writeCommand(0x80);		                        //kursor na początek
 //kolejne komórki ramu to hex: 40,48,50,58,60,68,70,78 czyli co 8 bajtów
}

void TLcd::defCel(void)
{
    defCustomChar(defDegre,HD44780_CGRAM_SET);
    defCustomChar(defSeparatorLeft,HD44780_CGRAM_SET_1);
    defCustomChar(defSeparatorRight,HD44780_CGRAM_SET_2);
    defCustomChar(defGOLDleft,HD44780_CGRAM_SET_3);
}
