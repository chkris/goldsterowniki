#include "adctemp.h"
#include <math.h>

TAdcTemp::TAdcTemp()
{

}
void TAdcTemp::init(void)
{
	DDRA &= ~(_BV(PA5) + _BV(PA6));//+ _BV(PA2) + _BV(PA3));// + _BV(PA4) + _BV(PA5) + _BV(PA6) + _BV(PA7));		    // PORT's as an input
	PORTA &= ~(_BV(PA5) + _BV(PA6));// + _BV(PA2) + _BV(PA3));// + _BV(PA4) + _BV(PA5) + _BV(PA6) + _BV(PA7));		    // Setting up the PORT's
                                //Wybranie wewnętrznego źródła odniesienia
	ADMUX |= _BV(REFS0);
	ADMUX &= ~_BV(REFS1);
                                //Wybranie sposobu zapisu wyniku z wyrównaniem do lewej (osiem starszych bitów wyniku w rejestrze ADCH)
	ADMUX |= _BV(ADLAR);
                                //Zezwolenie na konwersje
	ADCSRA |= _BV(ADEN);
                                //Wybranie częstotliwości dla taktowania przetwornika  (1/8 częstotliwosci zegara kontrolera)
	ADCSRA |= _BV(ADPS0);
	ADCSRA |= _BV(ADPS1);
	//ADCSRA |= _BV(ADPS2);

	buferCo = 0;
	buferCwu = 0;
	buferTime = 0;
	buferCount = 0;
}


 TTempError TAdcTemp::temp(int precision, unsigned char canal)
{
    unsigned int    wynik;
    unsigned char   counter;
    float           R2;
    float           deltaTA;
    float           T1;
    counter     =   precision;
    wynik       =   0;
    TTempError  temperatureWithWynik =  {0,0};

    ADMUX &= ~(_BV(MUX0) + _BV(MUX1) + _BV(MUX2) + _BV(MUX3) + _BV(MUX4));


    ADMUX |= canal;

    while(counter){
        ADCSRA |= _BV(ADSC);                     // Rozpoczęcie przetwarzania
        while(bit_is_set(ADCSRA,ADSC)){};		 // Oczekiwanie na zakończenie przetwarzania
        wynik = wynik + ADCW /(1 << 6);			 // Sumowanie wyników pomiarów dokonywanych w serii
        counter--;
    }
    wynik = wynik / precision;                   //liczenie średniej
    //od 390 do 700 właściwy zakres, poza zakresem błąd E2

    R2 = wynik * A / (5 - wynik * DU_10_BIT);
    deltaTA = sqrt(B - C * (RT25 - R2));
    T1 = 25 + (-D + deltaTA) / E;

    temperatureWithWynik.temp = T1;
    temperatureWithWynik.wynik = wynik;

    return temperatureWithWynik;

}

/*
TMyFloat TAdcTemp::dTTemp(int precision, unsigned char canal)
 {
    unsigned int    wynik = 0;
    unsigned char   counter = precision;
    TMyFloat        temp = {0,0};
    ADMUX &= ~(_BV(MUX0) + _BV(MUX1) + _BV(MUX2) + _BV(MUX3) + _BV(MUX4));


    ADMUX |= canal;

    while(counter){
        ADCSRA |= _BV(ADSC);                     // Rozpoczęcie przetwarzania
        while(bit_is_set(ADCSRA,ADSC)){};		 // Oczekiwanie na zakończenie przetwarzania
        wynik = wynik + ADCW /(1 << 6);			 // Sumowanie wyników pomiarów dokonywanych w serii
        counter--;
    }

    wynik = wynik / precision;                   //liczenie średniej
    checkWynik = wynik;
    wynik = (wynik - 459);                       //460 jednostek oznacza 0 C

    temp.all = (wynik * 54) / 100 - 4;             // dT na jednostkę 0.544 C

    //temp.all = wynik;
    return temp;
}
*/
