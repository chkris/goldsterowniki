#include <avr/interrupt.h>
#include <avr/wdt.h>

#include "avrlib/key.h"
#include "avrlib/timer1.h"


#include "avrlib/adctemp.h"
#include "menu.h"
#include "eeprom.h"


TAdcTemp adcTemp;
TMenu tMenu;
TKey key;
TFunc func;



#define MAX_OBROTY_WENTYLATORA  3
volatile unsigned char fineFan = 0;


//diody
//PD1
//PD0
//PA0
//PA4
//PA6
//PC7
//PC5


/*
#define DIODA_ON_1       PORTD |= _BV(1); DDRD |= _BV(1);
#define DIODA_OFF_1      PORTD &= ~_BV(1); DDRD &= ~_BV(1);

#define DIODA(NR) DIODA_ON_## NR
*/
// ze wzgl�du na eeprom musze t� funkcje w tym pliku umie�ci�
// bo przyda�oby si� �eby zmienne w eepromoie by�y widziane globalnie, we wszystkich plikach
// a extern nic nie pomaga



void serviceSetting(void)
{
    eeprom_write_byte(&eeTempZadana,55);
    eeprom_write_byte(&eeTempZalNadmuch ,40);
    eeprom_write_byte(&eePrzedmuchPracaMin ,15);
    eeprom_write_byte(&eePrzedmuchPrzerwaMin,5);
    eeprom_write_byte(&eeHisterezaNadmuch,2);
    eeprom_write_byte(&eeSilaNadmuch,3);
    eeprom_write_byte(&eeTempZalPompaCo,45);
    eeprom_write_byte(&eeHisterezaPompaCo,2);

#if PODAJNIK
     eeprom_write_byte(&eePracaFeederRozMin,2);
     eeprom_write_byte(&eePracaFeederRozSek,30);
     eeprom_write_byte(&eePrzerwaFeederRozMin,2);
     eeprom_write_byte(&eePrzerwaFeederRozSek,30);
#endif

#if POKOJOWKA
    eeprom_write_byte(&eePokojowka,0);
#endif

#if TEMP_CWU
    eeprom_write_byte(&eeTempZadanaCwu,50);
#endif


}


void show(void) {
    switch(pgm_read_byte(&lang[ tMenu.actualPos].func)) {

        case NO_FUNC:
            tMenu.dispalyMenu((char*)pgm_read_word(&lang[ tMenu.tMenuScreen.up].language[tMenu.actualSelectedLang]),
                        (char*)pgm_read_word(&lang[ tMenu.tMenuScreen.down].language[tMenu.actualSelectedLang]), tMenu.arrow,10);

        break;

        case ROZPALANIE:
            if(func.rozpalono == 1) {
                tMenu.displayYesOrNo((char*)pgm_read_word(&lang[ tMenu.actualPos].language[0]),func.rozpalanie,func.rozpalono,5);
            }
            if(func.rozpalono == 0){
                if(tMenu.menuFunc.key == KEY_UP) {
                    tMenu.ustRazpalanie = (tMenu.ustRazpalanie + 1)% 2;
                    tMenu.menuFunc.key = 0;
                }
                if(tMenu.menuFunc.key == KEY_DOWN) {
                    tMenu.ustRazpalanie = (tMenu.ustRazpalanie + 1)% 2;
                    tMenu.menuFunc.key = 0;
                }
                if(tMenu.menuFunc.key == KEY_OPTION) {

                    switch(tMenu.readEeprom) {
                        case 0:
                            tMenu.readEeprom = 1;
                        break;
                        case 1:
                            tMenu.lookCancel();
                            switch(tMenu.ustRazpalanie) {
                            case 0:func.rozpalanie = 0;
                            break;
                            case 1:func.rozpalanie = 1;func.feeder.state = 1;func.feeder.time.s = 0;func.feeder.time.m = 0;
                            break;
                            };
                            tMenu.readEeprom = 0;
                        break;
                    };
                    tMenu.blink = 0;
                    tMenu.menuFunc.key = 0;
                }

                tMenu.displayYesOrNo((char*)pgm_read_word(&lang[ tMenu.actualPos].language[tMenu.actualSelectedLang]),tMenu.ustRazpalanie,func.rozpalono,5);
            }
        break;

#if POKOJOWKA
        case UST_POKOJOWKA:
                if(tMenu.menuFunc.key == KEY_UP) {
                    tMenu.menuPok = (tMenu.menuPok  + 1) % 2;
                    tMenu.menuFunc.key = 0;
                }
                if(tMenu.menuFunc.key == KEY_DOWN) {
                    tMenu.menuPok  = (tMenu.menuPok  + 1) % 2;
                    tMenu.menuFunc.key = 0;
                }
                if(tMenu.menuFunc.key == KEY_OPTION) {
                    switch(tMenu.readEeprom) {
                        case 1:
                            eeprom_write_byte(&eePokojowka,tMenu.menuPok);
                            tMenu.lookCancel();
                            tMenu.readEeprom = 0;
                        break;
                        case 0:
                            tMenu.readEeprom = 1;
                        break;
                    };
                    tMenu.blink = 0;
                    tMenu.menuFunc.key = 0;
                }
                if(tMenu.menuFunc.key == KEY_OPTION) {
                    tMenu.lookCancel();
                }

                tMenu.displayYesOrNo((char*)pgm_read_word(&lang[ tMenu.actualPos].language[tMenu.actualSelectedLang]),tMenu.menuPok,0,5);

        break;
#endif

        case UST_SERWISOWE:

                if(tMenu.menuFunc.key == KEY_UP) {
                    tMenu.ustSerwisowe = (tMenu.ustSerwisowe  + 1) % 2;
                    tMenu.menuFunc.key = 0;
                }
                if(tMenu.menuFunc.key == KEY_DOWN) {
                    tMenu.ustSerwisowe  = (tMenu.ustSerwisowe  + 1) % 2;
                    tMenu.menuFunc.key = 0;
                }
                if(tMenu.menuFunc.key == KEY_OPTION) {
                    switch(tMenu.readEeprom) {
                        case 1:
                            if(tMenu.ustSerwisowe == 1) {
                                serviceSetting();
                                tMenu.setTempCo  = eeprom_read_byte(&eeTempZadana);//�eby zaktualizowa� t� warto��
#if TEMP_CWU
                                tMenu.setTempCwu = eeprom_read_byte(&eeTempZadanaCwu);//�eby zaktualizowa� t� warto��
#endif
                            }
                            tMenu.lookCancel();
                            tMenu.readEeprom = 0;
                        break;
                        case 0:
                            tMenu.readEeprom = 1;
                        break;
                    };
                    tMenu.blink = 0;
                    tMenu.menuFunc.key = 0;
                }
                if(tMenu.menuFunc.key == KEY_CANCEL) {
                    tMenu.lookCancel();
                }
                tMenu.displayYesOrNo((char*)pgm_read_word(&lang[ tMenu.actualPos].language[tMenu.actualSelectedLang]),tMenu.ustSerwisowe,0,5);

        break;

        case SET_TEMP_CO:
                     if(tMenu.menuFunc.key == KEY_UP) {
                        if(tMenu.firstTimePressed == 0) {
                            tMenu.setTempCo = eeprom_read_byte(&eeTempZadana);
                            tMenu.firstTimePressed = 1;
                        }
                        if(tMenu.setTempCo < pgm_read_byte(&tMenuFuncMaxMin[TEMP_ZADANA_CO].max)) {tMenu.setTempCo++;}
                        tMenu.menuFunc.key = 0;
                        tMenu.blink = 0;
                        tMenu.frequency = BLINK;
                        tMenu.save = 0;
                     }

                     if(tMenu.menuFunc.key == KEY_DOWN ) {
                        if(tMenu.firstTimePressed == 0) {
                            tMenu.setTempCo = eeprom_read_byte(&eeTempZadana);
                            tMenu.firstTimePressed = 1;
                        }
                        if(tMenu.setTempCo > pgm_read_byte(&tMenuFuncMaxMin[TEMP_ZADANA_CO].min)) { tMenu.setTempCo--; }
                        tMenu.menuFunc.key = 0;
                        tMenu.blink = 0;
                        tMenu.frequency = BLINK;
                        tMenu.save = 0;
                     }

                    if(tMenu.menuFunc.key == KEY_OPTION ) {
                         tMenu.blink = 0;
                         tMenu.frequency = 0;
                         if(tMenu.setTempCo != eeprom_read_byte(&eeTempZadana) && tMenu.save == 0) {
                            eeprom_write_byte(&eeTempZadana,tMenu.setTempCo);

                         }
                          tMenu.save = 1;
                        tMenu.menuFunc.key = 0;
                    }

                    //tMenu.setTempCo = tMenu.actTempCo;

                   // tMenu.actTempCo = adcTemp.checkWynik;

                    tMenu.displayCoCwu(SET_TEMP_CO, tMenu.actTempCo,tMenu.setTempCo,  tMenu.frequency, func.menuPok );


        break;

#if TEMP_CWU
        case SET_TEMP_CWU:
                     if(tMenu.menuFunc.key == KEY_UP) {
                        if(tMenu.firstTimePressed == 0) {
                            tMenu.setTempCwu = eeprom_read_byte(&eeTempZadanaCwu);
                            tMenu.firstTimePressed = 1;
                        }
                        if(tMenu.setTempCwu < pgm_read_byte(&tMenuFuncMaxMin[TEMP_ZADANA_CWU].max)) {tMenu.setTempCwu++;}
                        tMenu.menuFunc.key = 0;
                        tMenu.blink = 0;
                        tMenu.frequency = BLINK;
                        tMenu.save = 0;


                     }

                     if(tMenu.menuFunc.key == KEY_DOWN ) {
                        if(tMenu.firstTimePressed == 0) {
                            tMenu.setTempCwu = eeprom_read_byte(&eeTempZadanaCwu);
                            tMenu.firstTimePressed = 1;
                        }
                        if(tMenu.setTempCwu > pgm_read_byte(&tMenuFuncMaxMin[TEMP_ZADANA_CWU].min)) { tMenu.setTempCwu--; }
                        tMenu.menuFunc.key = 0;
                        tMenu.blink = 0;
                        tMenu.frequency = BLINK;
                        tMenu.save = 0;

                     }

                    if(tMenu.menuFunc.key == KEY_OPTION ) {
                         tMenu.blink = 0;
                         tMenu.frequency = 0;
                         if(tMenu.setTempCwu != eeprom_read_byte(&eeTempZadanaCwu) && tMenu.save == 0) {
                            eeprom_write_byte(&eeTempZadanaCwu,tMenu.setTempCwu);

                         }
                          tMenu.save = 1;
                        tMenu.menuFunc.key = 0;

                    }

                    tMenu.displayCoCwu(SET_TEMP_CWU, tMenu.actTempCwu,tMenu.setTempCwu,  tMenu.frequency,func.menuPok );
        break;
#endif

        case TEMP_ZAL_NADMUCH:
            //zapisywanie do eepromu
             tMenu.writeEeprom(&eeTempZalNadmuch, pgm_read_byte(&tMenuFuncMaxMin[TEMP_ZAL_NADMUCH].min), pgm_read_byte(&tMenuFuncMaxMin[TEMP_ZAL_NADMUCH].max));


             tMenu.displayValue((char*)pgm_read_word(&lang[ tMenu.actualPos].language[tMenu.actualSelectedLang]), tMenu.tmpEeprom,BLINK,"C");
        break;

        case PRZEDMUCH_PRACA:
            //zapisywanie do eepromu
             tMenu.writeEepromMinSek(&eePrzedmuchPracaMin,&eePrzedmuchPracaSek,0,59,0,59);
            //wy�wietlanie na ekranie
             tMenu.displayValueMinSek((char*)pgm_read_word(&lang[tMenu.actualPos].language[tMenu.actualSelectedLang]), tMenu.tmpEeprom,tMenu.tmpEeprom1,BLINK,tMenu.minSek);
        break;

        case PRZEDMUCH_PRZERWA:
            //zapisywanie do eepromu
             tMenu.writeEepromMinSek(&eePrzedmuchPrzerwaMin,&eePrzedmuchPrzerwaSek,0,59,0,59);
            //wy�wietlanie na ekranie
             tMenu.displayValueMinSek((char*)pgm_read_word(&lang[ tMenu.actualPos].language[tMenu.actualSelectedLang]), tMenu.tmpEeprom,tMenu.tmpEeprom1,BLINK,tMenu.minSek);
        break;

        case HISTEREZA_NADMUCH:
            //zapisywanie do eepromu
             tMenu.writeEeprom(&eeHisterezaNadmuch,
             pgm_read_byte(&tMenuFuncMaxMin[HISTEREZA_NADMUCH].min), pgm_read_byte(&tMenuFuncMaxMin[HISTEREZA_NADMUCH].max));
            //wy�wietlanie na ekranie
             tMenu.displayValue((char*)pgm_read_word(&lang[ tMenu.actualPos].language[tMenu.actualSelectedLang]), tMenu.tmpEeprom,BLINK,"C");
        break;

        case SILA_NADMUCH:
            //zapisywanie do eepromu
             tMenu.writeEeprom(&eeSilaNadmuch,
             pgm_read_byte(&tMenuFuncMaxMin[SILA_NADMUCH].min), pgm_read_byte(&tMenuFuncMaxMin[SILA_NADMUCH].max));
            //wy�wietlanie na ekranie
             tMenu.displayValue((char*)pgm_read_word(&lang[ tMenu.actualPos].language[tMenu.actualSelectedLang]), tMenu.tmpEeprom,BLINK,"bieg");
        break;

        case TEMP_ZAL_POMPA_CO:
            //zapisywanie do eepromu
             tMenu.writeEeprom(&eeTempZalPompaCo,
             pgm_read_byte(&tMenuFuncMaxMin[TEMP_ZAL_POMPA_CO].min), pgm_read_byte(&tMenuFuncMaxMin[TEMP_ZAL_POMPA_CO].max));
            //wy�wietlanie na ekranie
             tMenu.displayValue((char*)pgm_read_word(&lang[ tMenu.actualPos].language[tMenu.actualSelectedLang]), tMenu.tmpEeprom,BLINK,"C");
        break;

        case HISTEREZA_POMPA_CO:
            //zapisywanie do eepromu
             tMenu.writeEeprom(&eeHisterezaPompaCo,
             pgm_read_byte(&tMenuFuncMaxMin[HISTEREZA_POMPA_CO].min), pgm_read_byte(&tMenuFuncMaxMin[HISTEREZA_POMPA_CO].max));
            //wy�wietlanie na ekranie
             tMenu.displayValue((char*)pgm_read_word(&lang[ tMenu.actualPos].language[tMenu.actualSelectedLang]), tMenu.tmpEeprom,BLINK,"C");
        break;
#if TEMP_CWU
        case TEMP_ZAL_POMPA_CWU:
            //zapisywanie do eepromu
             tMenu.writeEeprom(&eeTempZalPompaCwu,
             pgm_read_byte(&tMenuFuncMaxMin[TEMP_ZAL_POMPA_CWU].min), pgm_read_byte(&tMenuFuncMaxMin[TEMP_ZAL_POMPA_CWU].max));
            //wy�wietlanie na ekranie
             tMenu.displayValue((char*)pgm_read_word(&lang[ tMenu.actualPos].language[tMenu.actualSelectedLang]), tMenu.tmpEeprom,BLINK,"C");
        break;

        case HISTEREZA_POMPA_CWU:
            //zapisywanie do eepromu
             tMenu.writeEeprom(&eeHisterezaPompaCwu,
             pgm_read_byte(&tMenuFuncMaxMin[HISTEREZA_POMPA_CWU].min), pgm_read_byte(&tMenuFuncMaxMin[HISTEREZA_POMPA_CWU].max));
            //wy�wietlanie na ekranie
             tMenu.displayValue((char*)pgm_read_word(&lang[ tMenu.actualPos].language[tMenu.actualSelectedLang]), tMenu.tmpEeprom,BLINK,"C");
        break;
#endif

#if PODAJNIK
        case PODAJNIK_ROZ_PRACA :
            //zapisywanie do eepromu
             tMenu.writeEepromMinSek(&eePracaFeederRozMin ,&eePracaFeederRozSek,0,59,0,59);
            //wy�wietlanie na ekranie
             tMenu.displayValueMinSek((char*)pgm_read_word(&lang[tMenu.actualPos].language[tMenu.actualSelectedLang]), tMenu.tmpEeprom,tMenu.tmpEeprom1,BLINK,tMenu.minSek);
        break;
        case PODAJNIK_ROZ_PRZER :
            //zapisywanie do eepromu

             tMenu.writeEepromMinSek(&eePrzerwaFeederRozMin ,&eePrzerwaFeederRozSek,0,59,0,59);
            //wy�wietlanie na ekranie
             tMenu.displayValueMinSek((char*)pgm_read_word(&lang[tMenu.actualPos].language[tMenu.actualSelectedLang]), tMenu.tmpEeprom,tMenu.tmpEeprom1,BLINK,tMenu.minSek);
        break;
#endif
/*
        case PRACA_RECZNA:
            tMenu.handWork = 1;

            swiatch(tMenu.handWork){
                case 1:
                    if(tMenu.menuFunc.key == KEY_UP) {
                        tMenu.arrow = UP;
                    }
                    if(tMenu.menuFunc.key == KEY_DOWN){
                        tMenu.arrow = DOWN;
                        tMenu.handWork = 2;
                    }
                    tMenu.dispalyMenu((char*)pgm_read_word(&tLangItem.pod[tMenu.actualSelectedLang]),(char*)pgm_read_word(&tLangItem.nad[tMenu.actualSelectedLang]), tMenu.arrow,10);
                break;
                case 2:
                    if(tMenu.menuFunc.key == KEY_UP) {
                        tMenu.arrow = UP;
                    }
                    if(tMenu.menuFunc.key == KEY_DOWN){
                        tMenu.arrow = DOWN;
                        tMenu.handWork = 2;
                    }
                    tMenu.dispalyMenu((char*)pgm_read_word(&tLangItem.pod[tMenu.actualSelectedLang]),(char*)pgm_read_word(&tLangItem.nad[tMenu.actualSelectedLang]), tMenu.arrow,10);
                break;





        break;
*/
    };

}


int main(void)
{
    key.init();
    adcTemp.init();
    tMenu.setTempCo = eeprom_read_byte(&eeTempZadana);
    #if TEMP_CWU
    tMenu.setTempCwu = eeprom_read_byte(&eeTempZadanaCwu);
    #endif



    tMenu.actualSelectedLang = 0;

    TCCR0 = INTP0_CLK1024;               //prescaler 64
    TCNT0 = 0;                           //reset counter
    TIMSK |= _BV(TOIE0);                 // enable TCNT0 overflow interrupt

    //TCCR2 = INTP0_CLK64;               //prescaler 64
    //TCNT2 = 216;                       //reset counter
    //TIMSK |= _BV(TOIE2);               // enable TCNT0 overflow interrupt

    TCCR1B = INTP0_CLK1;
    TCNT1H = 0;
    TCNT1L = 0;
    TIMSK |= (1 << TOIE1);


    GICR |= (1 << INT2);
    // set INT0 to edge detection
    MCUCR |= (1 << ISC2);
    // timer overflow interrupt enable
    TIMSK |= _BV(TOIE2);
//wy�witlanie wersji oprogramowania//
    LCD_ON;
tMenu.tLcd.clear();
tMenu.tLcd.goTo(0,0);
tMenu.tLcd.writeData(3);
tMenu.tLcd.writeText("GOLD ver. 0.4.6");
tMenu.tLcd.goTo(0,1);
tMenu.tLcd.writeText("www.goldster.eu");

for(int i = 0; i < 100; i++)
{
_delay_loop_2(50000);
}
tMenu.tLcd.clear();
//

    wdt_disable();
    sei();


    adcTemp.tempCo = (int)adcTemp.temp(10,5).temp;


    while (1)
    {

        if(func.rozpalanie == 1) {
            func.param.tempNadmuchStart = 0;
        }
        if(func.rozpalanie == 0) {
            func.param.tempNadmuchStart = eeprom_read_byte(&eeTempZalNadmuch);
        }




        #if POKOJOWKA
            func.menuPok = eeprom_read_byte(&eePokojowka);

            if(!(PIN_POKOJOWKA & _BV(NR_PIN_POKOJOWKA))) {
                func.pinPok = 1;
            } else {
                func.pinPok = 0;
            }


        //sprawdza stan pinu i przypisuje do func.pinPok = 1;
        #endif

        func.param.tempZadKotla     = eeprom_read_byte(&eeTempZadana);
        func.param.tempPumpCoStart  = eeprom_read_byte(&eeTempZalPompaCo);
        func.param.hisKotla         = eeprom_read_byte(&eeHisterezaNadmuch);
        func.param.hisPompaCo       = eeprom_read_byte(&eeHisterezaPompaCo );
        func.param.przerwaNadPodMin = eeprom_read_byte(&eePrzedmuchPrzerwaMin);
        func.param.pracaNadPodMin   = eeprom_read_byte(&eePrzedmuchPracaMin );
        func.param.przerwaNadPodSek = eeprom_read_byte(&eePrzedmuchPrzerwaSek);
        func.param.pracaNadPodSek   = eeprom_read_byte(&eePrzedmuchPracaSek );
        func.param.tempAktKotla     = adcTemp.tempCo;
#if TEMP_CWU
        func.param.tempAktCwu        = adcTemp.tempCwu;
        func.param.tempPompaCwuStart = eeprom_read_byte(&eeTempZalPompaCwu );
        func.param.tempPompaCwuStop  = eeprom_read_byte(&eeTempZadanaCwu );
        func.param.hisPompaCwu       = eeprom_read_byte(&eeHisterezaPompaCwu );
        func.pompaCwu(func.param);
#endif
#if PODAJNIK

        func.param.pracaFeederRozMin     = eeprom_read_byte(&eePracaFeederRozMin) ;
        func.param.pracaFeederRozSek     = eeprom_read_byte(&eePracaFeederRozSek );

        func.param.przerwaFeederRozMin   = eeprom_read_byte(&eePrzerwaFeederRozMin);
        func.param.przerwaFeederRozSek   = eeprom_read_byte(&eePrzerwaFeederRozSek);


#endif

//************************************PRACA RECZNA******************************************
#if TEMP_CWU
            if(tMenu.actualPos >= HAND_WORK_FAN_POS  && tMenu.actualPos <= HAND_WORK_POMPA_CWU){
                switch(tMenu.handWork){
                    case 0:
                        func.fan.state=0;
                        func.pumpCo.state=0;
                        func.pumpCwu.state=0;
                        tMenu.handWork=1;
                        tMenu.menuFunc.key = 0;
                    break;
                }
                switch(tMenu.actualPos){
                case HAND_WORK_POMPA_CWU :
                    if(tMenu.menuFunc.key == KEY_OPTION ) {
                        switch(func.feeder.state){
                            case 0:func.feeder.state = 1;
                            break;
                            case 1:func.feeder.state = 0;
                            break;
                        }
                    tMenu.menuFunc.key = 0;
                    }
                break;
                case HAND_WORK_FAN_POS:
                    if(tMenu.menuFunc.key == KEY_OPTION ) {
                        switch(func.fan.state){
                            case 0:func.fan.state = 1;
                            break;
                            case 1:func.fan.state = 0;
                            break;
                        }
                    tMenu.menuFunc.key = 0;
                    }
                break;
                case HAND_WORK_PUMP_CO_POS :
                    if(tMenu.menuFunc.key == KEY_OPTION ) {
                        switch(func.pumpCo.state){
                            case 0:func.pumpCo.state = 1;
                            break;
                            case 1:func.pumpCo.state = 0;
                            break;
                        }
                        tMenu.menuFunc.key = 0;
                    }
                break;
                }
            }
            if(tMenu.actualPos < HAND_WORK_FAN_POS  || tMenu.actualPos > HAND_WORK_POMPA_CWU ){
                func.nadmuch(func.param);
                func.pompaCo(func.param);
                func.pompaCwu(func.param);
                tMenu.handWork = 0;
            }
#endif
#if PODAJNIK
            if(tMenu.actualPos >= HAND_WORK_FAN_POS  && tMenu.actualPos <= HAND_WORK_PODAJNIK ){
                switch(tMenu.handWork){
                    case 0:
                        func.feeder.state=0;
                        func.fan.state=0;
                        func.pumpCo.state=0;
                        tMenu.handWork=1;
                        tMenu.menuFunc.key = 0;
                    break;
                }
                switch(tMenu.actualPos){
                case HAND_WORK_PODAJNIK :
                    if(tMenu.menuFunc.key == KEY_OPTION ) {
                        switch(func.feeder.state){
                            case 0:func.feeder.state = 1;
                            break;
                            case 1:func.feeder.state = 0;
                            break;
                        }
                    tMenu.menuFunc.key = 0;
                    }
                break;
                case HAND_WORK_FAN_POS:
                    if(tMenu.menuFunc.key == KEY_OPTION ) {
                        switch(func.fan.state){
                            case 0:func.fan.state = 1;
                            break;
                            case 1:func.fan.state = 0;
                            break;
                        }
                    tMenu.menuFunc.key = 0;
                    }
                break;
                case HAND_WORK_PUMP_CO_POS :
                    if(tMenu.menuFunc.key == KEY_OPTION ) {
                        switch(func.pumpCo.state){
                            case 0:func.pumpCo.state = 1;
                            break;
                            case 1:func.pumpCo.state = 0;
                            break;
                        }
                        tMenu.menuFunc.key = 0;
                    }
                break;
                }
            }
            if(tMenu.actualPos<HAND_WORK_FAN_POS  || tMenu.actualPos>HAND_WORK_PODAJNIK ){
                func.nadmuch(func.param);
                func.pompaCo(func.param);
                tMenu.handWork=0;
            }
#endif
#if ONLY_FAN && !TEMP_CWU
            if(tMenu.actualPos >= HAND_WORK_FAN_POS && tMenu.actualPos <= HAND_WORK_PUMP_CO_POS){
                switch(tMenu.handWork){
                    case 0:
                        func.feeder.state=0;
                        func.fan.state=0;
                        func.pumpCo.state=0;
                        tMenu.handWork=1;
                        tMenu.menuFunc.key = 0;
                    break;
                }
                switch(tMenu.actualPos){
                case HAND_WORK_FAN_POS:
                    if(tMenu.menuFunc.key == KEY_OPTION ) {
                        switch(func.fan.state){
                            case 0:func.fan.state = 1;
                            break;
                            case 1:func.fan.state = 0;
                            break;
                        }
                    tMenu.menuFunc.key = 0;
                    }
                break;
                case HAND_WORK_PUMP_CO_POS:
                    if(tMenu.menuFunc.key == KEY_OPTION ) {
                        switch(func.pumpCo.state){
                            case 0:func.pumpCo.state = 1;
                            break;
                            case 1:func.pumpCo.state = 0;
                            break;
                        }
                        tMenu.menuFunc.key = 0;
                    }
                break;
                }
            }
            if(tMenu.actualPos < HAND_WORK_FAN_POS || tMenu.actualPos > HAND_WORK_PUMP_CO_POS){
                func.nadmuch(func.param);
                func.pompaCo(func.param);
                tMenu.handWork=0;
            }

#endif
//************************************KONIEC - PRACA RECZNA******************************************//

#if ONLY_FAN
        if(pgm_read_byte(&lang[ tMenu.actualPos].func) == SET_TEMP_CO && tMenu.save == 0) {
            func.fan.state = 0;
        }
#endif

//wyjscie z menu po 20 sek od naci�ni�cia przycisku do wyswietlania temperatury aktualnej CO
if(tMenu.outOfMenu >= 2000){
    tMenu.actualPos = 0;
    #if TEMP_CWU
    tMenu.actualPos = 1;
    #endif
}
//

     switch(func.ERROR) {
            case NO_ERROR :

                switch (key.lastKey)
                {
                case KEY_UP:
                    tMenu.lookUp();
                    tMenu.outOfMenu = 0;
                    key.lastKey = KEY_NO_PRESS;// nie wolno zapomnie� bo wykona ten kod 100 razy
                break;
                case KEY_DOWN:
                    tMenu.lookDown();
                    tMenu.outOfMenu = 0;
                    key.lastKey = KEY_NO_PRESS;// nie wolno zapomnie� bo wykona ten kod 100 razy
                break;
                case KEY_OPTION:
                    tMenu.lookOption();
                    tMenu.outOfMenu = 0;
                    key.lastKey = KEY_NO_PRESS;// nie wolno zapomnie� bo wykona ten kod 100 razy
                break;
                case KEY_CANCEL:
                    tMenu.lookCancel();
                    tMenu.outOfMenu = 0;
                    key.lastKey = KEY_NO_PRESS; // nie wolno zapomnie� bo wykona ten kod 100 razy
                break;
                };

                show();

               // DIODA_3_OFF;
            break;
            case ZA_DUZA_TEMP_KOTLA :
              //  DIODA_3_ON;
                if(tMenu.buzerOffAlram == 0){BUZER_ON;}
                if(key.lastKey != KEY_NO_PRESS){BUZER_OFF;tMenu.buzerOffAlram = 1;}
                tMenu.displayValue((char*)pgm_read_word(&tLangItem.ALARMCO[tMenu.actualSelectedLang]), adcTemp.tempCo,BLINK,"C");
                func.pumpCo.state = 1;
                func.fan.state = 0;
                #if PODAJNIK
                func.feeder.state = 0;
                #endif
            break;
            case BLAD_CZUJNIKA_CO :
              //  DIODA_3_ON;
                if(tMenu.buzerOffAlram == 0){BUZER_ON;}
                if(key.lastKey != KEY_NO_PRESS){BUZER_OFF;tMenu.buzerOffAlram = 1;}
                tMenu.tLcd.goTo(0,0);
                tMenu.tLcd.writeFlash((char*)pgm_read_word(&tLangItem.ALARMCOU[tMenu.actualSelectedLang]));
                tMenu.tLcd.goTo(0,1);
                tMenu.tLcd.writeFlash((char*)pgm_read_word(&tLangItem.CLEAR[tMenu.actualSelectedLang]));
                func.pumpCo.state = 1;
                func.fan.state = 0;
                #if PODAJNIK
                func.feeder.state = 0;
                #endif
            break;
#if PODAJNIK
            case BLAD_CZUJNIKA_POD :
                if(tMenu.buzerOffAlram == 0){BUZER_ON;}
                if(key.lastKey != KEY_NO_PRESS){BUZER_OFF;tMenu.buzerOffAlram = 1;}
                tMenu.tLcd.goTo(0,0);
                tMenu.tLcd.writeFlash((char*)pgm_read_word(&tLangItem.ALARMPODU[tMenu.actualSelectedLang]));
                tMenu.tLcd.goTo(0,1);
                tMenu.tLcd.writeFlash((char*)pgm_read_word(&tLangItem.CLEAR[tMenu.actualSelectedLang]));
                func.pumpCo.state = 1;
                func.fan.state = 0;
                func.feeder.state = 0;
            break;
            case ZA_DUZA_TEMP_POD :
                if(tMenu.buzerOffAlram == 0){BUZER_ON;}
                if(key.lastKey != KEY_NO_PRESS){BUZER_OFF;tMenu.buzerOffAlram = 1;}
                tMenu.displayValue((char*)pgm_read_word(&tLangItem.ALARMPOD[tMenu.actualSelectedLang]), adcTemp.tempCwu,BLINK,"C");
                func.pumpCo.state = 1;
                func.fan.state = 0;
                func.feeder.state = 1;
            break;


#endif

        }

        switch(func.fan.state) {
            case 0:
                MOC_OFF;
                _delay_loop_2(5);
                //DIODA_2_OFF;
                DIODA_FAN_OFF;FAN_OFF;
            break;
            case 1:
                //DIODA_2_ON;
                DIODA_FAN_ON;FAN_ON;
            break;
        };
#if PODAJNIK
         switch(func.feeder.state) {
            case 0:
                DIODA_FEEDER_OFF;FEEDER_OFF;
            break;
            case 1:
                DIODA_FEEDER_ON;FEEDER_ON;
            break;
        };
#endif
        switch(func.pumpCo.state) {
            case 0:// DIODA_1_OFF;
            DIODA_PUMP_CO_OFF;PUMP_CO_OFF;
            break;
            case 1: //DIODA_1_ON;
            DIODA_PUMP_CO_ON;PUMP_CO_ON;
            break;
        }
#if TEMP_CWU
        switch(func.pumpCwu.state) {
            case 0: DIODA_FEEDER_OFF;FEEDER_OFF;//odnosi si� dopompy CWU
            break;
            case 1: DIODA_FEEDER_ON;FEEDER_ON;//odnosi si� dopompy CWU
            break;
        }

#endif





        //mierzenie temperatury
        if(adcTemp.buferTime == BUFER_TIME) {
            adcTemp.buferCo = adcTemp.buferCo + (int)adcTemp.temp(10,5).temp;
            adcTemp.wynikCo = adcTemp.temp(10,5).wynik;

            #if TEMP_CWU
                adcTemp.buferCwu = adcTemp.buferCwu + (int)adcTemp.temp(10,6).temp;
                adcTemp.wynikCwuPod = 400;//adcTemp.temp(10,6).wynik;
            #endif

            #if PODAJNIK
                adcTemp.buferCwu = adcTemp.buferCwu + (int)adcTemp.temp(10,6).temp;
                adcTemp.wynikCwuPod = 400;//adcTemp.temp(10,6).wynik;

            #endif

            adcTemp.buferCount++;
            adcTemp.buferTime = 0;
            if(adcTemp.buferCount == BUFER_COUNT) {
                adcTemp.buferCount = 0;
                #if TEMP_CWU
                    adcTemp.tempCwu = 50;//(int)(adcTemp.buferCwu / BUFER_COUNT);
                    adcTemp.buferCwu = 0;
                    if(adcTemp.wynikCwuPod < 390 || adcTemp.wynikCwuPod > 700 ){
                           func.ERROR = BLAD_CZUJNIKA_CWU;
                    }
                #endif

                #if PODAJNIK
                    adcTemp.tempCwu = 50;//(int)(adcTemp.buferCwu / BUFER_COUNT);
                    adcTemp.buferCwu = 0;
                    if(adcTemp.wynikCwuPod < 390 || adcTemp.wynikCwuPod > 700 ){
                           func.ERROR = BLAD_CZUJNIKA_POD;
                    }
                    if(adcTemp.tempCwu >= ALARM_POD) {func.ERROR = ZA_DUZA_TEMP_POD;}
                #endif
                adcTemp.tempCo = (int)(adcTemp.buferCo / BUFER_COUNT);
                adcTemp.buferCo  = 0;
                if(adcTemp.wynikCo < 390 || adcTemp.wynikCo > 700 ){
                       func.ERROR = BLAD_CZUJNIKA_CO;
                }

            }

        }

        tMenu.actTempCo = (unsigned int)adcTemp.tempCo;

        #if TEMP_CWU
        tMenu.actTempCwu = (unsigned char)adcTemp.tempCwu;
        #endif


                    //tMenu.setTempCo = tMenu.actTempCo;

                   // tMenu.actTempCo = adcTemp.checkWynik;

                    //tMenu.displayCoCwu(SET_TEMP_CO, tMenu.actTempCo,tMenu.setTempCo,  tMenu.frequency );


        fineFan = eeprom_read_byte(&eeSilaNadmuch);
        if(fineFan == MAX_OBROTY_WENTYLATORA) {
            MOC_ON;
        }
    }
    return 0;
}



SIGNAL(SIG_INTERRUPT2)
{
    if(fineFan != MAX_OBROTY_WENTYLATORA){
        MOC_OFF;
    }
    TCNT1H = 157 + fineFan * 14;
    TCNT1L = 255;
}


SIGNAL(SIG_OVERFLOW1)
{
    if(fineFan != MAX_OBROTY_WENTYLATORA){
        MOC_ON;
        TCNT1H = 0;
        TCNT1L = 0;
    }
}

volatile unsigned char keyTestDelay = 0;

ISR(SIG_OVERFLOW0)
{
    TCNT0 = 216;
    if(keyTestDelay++ > 7) {
        key.test();
        keyTestDelay = 0;
    }

    if(adcTemp.buferTime < BUFER_TIME){
       adcTemp.buferTime++;
    }

    tMenu.outOfMenu++;

    func.mesureTime(&func.feeder.time);
    func.mesureTime(&func.fan.time);
    func.mesureTime(&func.pumpCo.time);

    if(key.buzerOff == 4){
        BUZER_OFF;
    }
    key.buzerOff++;
}
