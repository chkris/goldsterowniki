#ifndef _EEPROM_H__INCLUDED_
#define _EEPROM_H__INCLUDED_

unsigned char eeHOLE01             __attribute__((section(".eeprom"))) = 60;  // C
unsigned char eeHOLE02             __attribute__((section(".eeprom"))) = 60;  // C

unsigned char eeTempZadana             __attribute__((section(".eeprom"))) = 60;  // C

unsigned char eeTempZalNadmuch         __attribute__((section(".eeprom"))) = 40;  // C
//NADMUCH
unsigned char eePrzedmuchPracaMin      __attribute__((section(".eeprom"))) = 0;  // sek.
unsigned char eePrzedmuchPracaSek      __attribute__((section(".eeprom"))) = 1;  // sek.

unsigned char eePrzedmuchPrzerwaMin    __attribute__((section(".eeprom"))) = 0;   // min.
unsigned char eePrzedmuchPrzerwaSek    __attribute__((section(".eeprom"))) = 1;   // min.
//NADMUCH
#if PODAJNIK
unsigned char eePracaFeederRozMin      __attribute__((section(".eeprom"))) = 0;  // min
unsigned char eePracaFeederRozSek      __attribute__((section(".eeprom"))) = 1;  // sek.
unsigned char eePrzerwaFeederRozMin    __attribute__((section(".eeprom"))) = 0;  // min
unsigned char eePrzerwaFeederRozSek    __attribute__((section(".eeprom"))) = 1;  // sek.

#endif
unsigned char eeHisterezaNadmuch       __attribute__((section(".eeprom"))) = 2;   // C
unsigned char eeSilaNadmuch            __attribute__((section(".eeprom"))) = 1;   // bieg
unsigned char eeTempZalPompaCo         __attribute__((section(".eeprom"))) = 10;  // C
unsigned char eeHisterezaPompaCo       __attribute__((section(".eeprom"))) = 3;   // C
unsigned char eePokojowka              __attribute__((section(".eeprom"))) = 0;   // C

#if TEMP_CWU
unsigned char eeTempZalPompaCwu         __attribute__((section(".eeprom"))) = 20;  // C
unsigned char eeHisterezaPompaCwu       __attribute__((section(".eeprom"))) = 3;   // C
unsigned char eeTempZadanaCwu           __attribute__((section(".eeprom"))) = 50;  // C
#endif

#endif
