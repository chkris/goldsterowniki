/*
 *      menu.cpp
 *      avrlib
 *      Created by Krzysztof Cholewka on April 2009
 *      Copyright (c). All rights reserved.
 */

/* menu is a interface for TMenu class
 * contained structute of menu set as array,
 * but behave like tree
 */




#ifndef _MENU_H__INCLUDED_
#define _MENU_H__INCLUDED_

#include "function.h"
#include "avrlib/hd44780.h"
#include "avrlib/key.h"

#include <avr/eeprom.h>
#include <avr/pgmspace.h>




#define TIME 1
#define CELCJUS 0
#define BLINK   8
#define UP      0
#define DOWN    1

#define MAX_LANG 2

//definicje poziom�w w menu
#define START_MENU  2
#define MAIN_MENU   1
#define END         0

//definicje, kt�re b�d� dzia�a� jak maszyna stan�w

#define NO_FUNC                 253
#define SET_TEMP_CO             254
#define SET_TEMP_CWU            255

#define START_FUNC              0
#define END_FUNC                NO_FUNC - 1
//tu maxymalne i minimalne wartoe�i w TMenuFuncMaxMin
#define TEMP_ZADANA_CWU         0
#define TEMP_ZADANA_CO          1
#define TEMP_ZAL_NADMUCH        2
#define PRZEDMUCH_PRACA         3
#define PRZEDMUCH_PRZERWA       4
#define HISTEREZA_NADMUCH       5
#define SILA_NADMUCH            6
#define TEMP_ZAL_POMPA_CO       7
#define HISTEREZA_POMPA_CO      8
#define TEMP_ZAL_POMPA_CWU      9
#define HISTEREZA_POMPA_CWU     10
#define PODAJNIK_ROZ_PRACA      11
#define PODAJNIK_ROZ_PRZER      12
//tu maxymalne i minimalne wartoe�i w TMenuFuncMaxMin
#define ROZPALANIE              13
#define UST_SERWISOWE           14
#define UST_POKOJOWKA           15

#define PRACA_RECZNA 16



struct TMenuFuncMaxMin {
    unsigned char min;
    unsigned char max;
};

static struct TMenuFuncMaxMin tMenuFuncMaxMin[] __attribute__ ((progmem))=
{
    {40,65},
    {40,80},
    {30,50},
    {1 ,99},
    {1 ,59},
    {1 ,5 },
    {1 ,3 },
    {10,55},
    {1 ,5 },
    {30,55},
    {1 ,5 },
};



struct TMenuFunc {
    unsigned char key;
};

struct TLangItem
{
    PGM_P zadCo[MAX_LANG];
    PGM_P aktCo[MAX_LANG];
    PGM_P zadCwu[MAX_LANG];
    PGM_P aktCwu[MAX_LANG];
    PGM_P tak[MAX_LANG];
    PGM_P nie[MAX_LANG];
    PGM_P pod[MAX_LANG];
    PGM_P nad[MAX_LANG];
    PGM_P pCo[MAX_LANG];
    PGM_P pCwu[MAX_LANG];
    PGM_P ALARMCO[MAX_LANG];
    PGM_P ALARMPOD[MAX_LANG];
    PGM_P ALARMCOU[MAX_LANG];
    PGM_P ALARMPODU[MAX_LANG];
    PGM_P CLEAR[MAX_LANG];
};

static struct TLangItem tLangItem __attribute__ ((progmem))=
{
    {pol_zadCo,en_zadCo},
    {pol_aktCo,en_aktCo},
    {pol_zadCwu,en_zadCwu},
    {pol_aktCwu,en_aktCwu},
    {pol_tak,en_tak},
    {pol_nie,en_nie},
    {pol_podajnik,pol_podajnik},
    {pol_nadmuch,pol_nadmuch},
    {pol_pompaCo,pol_pompaCo},
    {pol_pompaCwu,pol_pompaCwu},
    {pol_ALARMCO,pol_ALARMCO},
    {pol_ALARMPOD,pol_ALARMPOD},
    {pol_ALARMCOU,pol_ALARMCO},
    {pol_ALARMPODU,pol_ALARMPOD},
    {pol_CLEAR,pol_ALARMPOD},
};


struct TLang
{
    PGM_P language[2] ;
    unsigned char level;
    unsigned char func;
};




static struct TLang lang[] __attribute__ ((progmem))=
{
    #if TEMP_CWU
    {{pol_rozpalanie,pol_rozpalanie},MAIN_MENU,SET_TEMP_CWU},
    #endif

    {{pol_rozpalanie,pol_rozpalanie},MAIN_MENU,SET_TEMP_CO},

            {{pol_rozpalanie,en_rozpalanie},2,NO_FUNC},                     {{pol_rozpalanie,en_rozpalanie},3,ROZPALANIE},

            {{pol_pracaReczna,pol_pracaReczna},2,NO_FUNC},

                    {{pol_nadmuch,en_przedmuchPrzerwa},3,NO_FUNC},
                    {{pol_pompaCo,en_histereza},3,NO_FUNC},
                    #if PODAJNIK
                    {{pol_podajnik,en_przedmuchPraca},3,NO_FUNC},
                    #endif
                    #if TEMP_CWU
                    {{pol_pompaCwu,en_silaNadmuchu},3,NO_FUNC},
                    #endif

            {{pol_tempZal,en_tempZal},2,NO_FUNC},                           {{pol_tempZal,en_tempZal},3,TEMP_ZAL_NADMUCH},
            {{pol_histerezaKotla,en_histereza},2,NO_FUNC},                  {{pol_histerezaKotla,en_histereza},3,HISTEREZA_NADMUCH},
            {{pol_ustNadmuch,en_ustNadmuch},2,NO_FUNC},

                    {{pol_przedmuchPraca,en_przedmuchPraca},3,NO_FUNC},     {{pol_przedmuchPraca,en_przedmuchPraca},4,PRZEDMUCH_PRACA},
                    {{pol_przedmuchPrzerwa,en_przedmuchPrzerwa},3,NO_FUNC}, {{pol_przedmuchPrzerwa,en_przedmuchPrzerwa},4,PRZEDMUCH_PRZERWA},
                    {{pol_silaNadmuchu,en_silaNadmuchu},3,NO_FUNC},         {{pol_silaNadmuchu,en_silaNadmuchu},4,SILA_NADMUCH},

#if PODAJNIK
            {{pol_ustPodajnik,en_ustPodajnik},2,NO_FUNC},

                    {{pol_rozpalaniePraca,en_rozpalaniePraca},3,NO_FUNC},        {{pol_rozpalaniePraca,en_rozpalaniePraca},4,PODAJNIK_ROZ_PRACA},
                    {{pol_rozpalaniePrzerwa,en_rozpalaniePrzerwa},3,NO_FUNC},    {{pol_rozpalaniePrzerwa,en_rozpalaniePrzerwa},4,PODAJNIK_ROZ_PRZER},
#endif

            {{pol_ustPompaCO,en_ustPompaCO},2,NO_FUNC},

                    {{pol_tempZalCo,en_tempZal},3,NO_FUNC},                   {{pol_tempZal,en_tempZal},4,TEMP_ZAL_POMPA_CO},
                    {{pol_histereza,en_histereza},3,NO_FUNC},               {{pol_histereza,en_histereza},4,HISTEREZA_POMPA_CO},

            #if TEMP_CWU
            {{pol_ustPompaCWU,en_ustPompaCWU},2,NO_FUNC},

                    {{pol_tempZalCo,en_tempZal},3,NO_FUNC},                   {{pol_tempZalCo,en_tempZal},4,TEMP_ZAL_POMPA_CWU},
                    {{pol_histereza,en_histereza},3,NO_FUNC},               {{pol_histereza,en_histereza},4,HISTEREZA_POMPA_CWU},
            #endif


            #if POKOJOWKA
            {{pol_pokojowka,en_pokojowka},2,NO_FUNC},                       {{pol_pokojowka,en_pokojowka},3,UST_POKOJOWKA},
            #endif

            {{pol_ustSerwis,en_ustSerwis},2,NO_FUNC},                       {{pol_ustSerwis,en_ustSerwis},3,UST_SERWISOWE},



    {{pol_rozpalanie,pol_rozpalanie},END,NO_FUNC}

};

#define HAND_WORK_FAN_POS     4
#define HAND_WORK_PUMP_CO_POS 5
#define HAND_WORK_PODAJNIK    6
#if TEMP_CWU
#define HAND_WORK_FAN_POS     5
#define HAND_WORK_PUMP_CO_POS 6
#define HAND_WORK_POMPA_CWU   7
#endif


// struktura, kt�ra wy�wietla na lcd 2x16 aktualnie zapami�tany index tablicy lang[]
struct TMenuScreen {
    unsigned char up;
    unsigned char down;
};


class TMenu
{


    public:



        TLcd tLcd;
        TMenu();

        unsigned char handWork;
        unsigned char buzerOffAlram;

        unsigned char tmpEeprom1;
        unsigned char tmpEeprom;
        unsigned char readEeprom;
        unsigned char ustSerwisowe;

        unsigned char actualSelectedLang;

        unsigned char ustRazpalanie; // zmiena przy wyborzy czy rozpala� czy nie w menu nie w function.cppS
        unsigned char menuPok;

        //************** wy�wietlanie na wy�wietlaczu lcd 2x16 **********************//
        unsigned char blink;
        unsigned char frequency;
        void displayValue(char* firstLine,unsigned char value, unsigned char freuquency, char* unit);
        void displayValueMinSek(char* firstLine,unsigned char value, unsigned char value1, unsigned char freuquency, char minSek);
        void dispalyMenu(char* firstLine,char* secondLine, char selected, char freuquency);
        void displayYesOrNo(char* firstLine,int value, unsigned char roz, unsigned char freuquency);

        #if TEMP_CWU
        unsigned char actTempCwu; //zmienna globalna do wy�wietlania temperetury aktualnej
        unsigned char setTempCwu; //do przechowywania zawarto�ci eepromu
        #endif

        unsigned int actTempCo; //zmienna globalna do wy�wietlania temperetury aktualnej
        unsigned char setTempCo; //do przechowywania zawarto�ci eepromu

        unsigned char firstTimePressed;
        unsigned char save; // chroni przed zapisyem wielokrotnym do eepromu, dopiero jak zostanie naci�ni�ty przycisk
        unsigned char minSek;

        volatile unsigned int outOfMenu;
        void displayCoCwu(unsigned char coOrCwu, unsigned int actualTemp, unsigned char setTemp,unsigned char freuquency,unsigned char pok);
        //void show(void);//przeniesiona do pliku main.cpp
        //************** wy�wietlanie na wy�wietlaczu lcd 2x16 **********************//

        //************** wyszukiwanie w menu odpowiednich warto�ci*******************//
        unsigned char arrow;
        TMenuFunc menuFunc;
        unsigned char actualPos;
        TMenuScreen tMenuScreen; //b�dzie przechowywa� nr dla pierwszej lini i drugiej, kt�re musi wyszuka�
        void lookDown(void);
        void lookUp(void);
        void lookOption(void);
        void lookCancel(void);
        //************** wyszukiwanie w menu odpowiednich warto�ci******************//

        void writeEeprom(uint8_t* adress, unsigned char min, unsigned char max);
        //
        void writeEepromMinSek(uint8_t* adress,uint8_t* adress1, unsigned char min, unsigned char max,unsigned char min1, unsigned char max1);

};

#endif
