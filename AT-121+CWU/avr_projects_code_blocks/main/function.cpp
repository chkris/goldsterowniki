#include "function.h"

TFunc::TFunc()
{
    rozpalanie = 0;
    fan.zeroTime = 0;
    fan.state = 0;

}

void TFunc::mesureTime(struct TTime* time)
{
    if(time->ms < 999) {
        time->ms = time->ms + 10;
    } else {
        time->ms = 0;
        if(++time->s % 60 == 0) {
            if(time->s == 120) {time->s = 0;}
            if(++time->m % 60 == 0) {
                if(time->m == 120) {time->m = 0;}
                if(++time->h % 24 == 0) {
                    time->h = 0;
                    time->d++;
                }
            }
        }
    }
}
void TFunc::setTime(u8 d,u8 h, u8 m, u8 s, u8 ms, struct TTime* time)
{
    time->d = d;
    time->h = h;
    time->m = m;
    time->s = s;
    time->ms = ms;
}

void TFunc::toggle(unsigned int work, unsigned int pause,struct TTime* time, char* state, char* zeroTime)
{
    switch(*state) {
    case 0:
        if(*zeroTime == 0) {setTime(0,0,0,0,0,time); *zeroTime = 1;}
        if(time->m >= pause) { *state = 1; *zeroTime = 0;}
    break;
    case 1:
        if(*zeroTime == 0) { setTime(0,0,0,0,0,time); *zeroTime = 1;}
        if(time->s >= work) { *state = 0; *zeroTime = 0;}
    break;
    }
}


void TFunc::pompaCo(TParameters param)
{
    //funkcja antystop


    //przeciw zamarzaniu wody, je�li temperatura jest mnjiejsza od 5 stopni

    if(param.tempAktKotla <= 5) {
        pumpCo.state = 1;
    }

    if((param.tempAktKotla > 5) && (param.tempAktKotla <= param.tempPumpCoStart - param.hisKotla)) {
        pumpCo.state = 0;
        hisPumpCo = 0;
        if(pumpCo.time.d >= ANTY_STOP_PRZERWA) {
            pumpCo.state = 1;
            if(pumpCo.time.m > ANTY_STOP_PRACA) {
                setTime(0,0,0,0,0,&pumpCo.time);
            }
        }
    }


    if(param.tempAktKotla > param.tempPumpCoStart - param.hisKotla) {
        switch(hisPumpCo){
            case 0 :
                pumpCo.state = 0;
                break;
            case 1:
                pumpCo.state = 1;
                #if POKOJOWKA
                    if(pinPok && menuPok) {
                        pumpCo.state = 0;
                    }
                    //tu sprawdza, je�li pokoj�wka aktaywowana w menu to je�li pin w stanie wysokim to wy��cza pompe
                #endif
                break;
        };
    }


    if(param.tempAktKotla >= param.tempPumpCoStart){
         hisPumpCo = 1;
         pumpCo.state = 1;
    }


}


void TFunc::pompaCwu(TParameters param)
{
    if(param.tempAktKotla > param.tempAktCwu){
        if(param.tempAktCwu <= param.tempPompaCwuStart - param.hisKotla) {
            pumpCwu.state = 0;
            hisPumpCwuStart = 0;
        }

        if((param.tempAktCwu > param.tempPompaCwuStart - param.hisKotla)
        && (param.tempAktCwu < param.tempPompaCwuStart))
        {
            switch(hisPumpCwuStart){
                case 0 :
                    pumpCwu.state = 0;
                    break;
                case 1:
                    pumpCwu.state = 1;
                    break;
            };
        }

        if((param.tempAktCwu >=  param.tempPompaCwuStart) && (param.tempAktCwu  <= param.tempPompaCwuStop - param.hisKotla)) {
            pumpCwu.state = 1;
            hisPumpCwu = 0;
            hisPumpCwuStart = 1;
        }

        if((param.tempAktCwu >= param.tempPompaCwuStart) && (param.tempAktCwu > param.tempPompaCwuStop - param.hisKotla) && (param.tempAktCwu < param.tempPompaCwuStop)){
            switch(hisPumpCwu){
                case 0 :
                    pumpCwu.state = 1;
                    break;
                case 1:
                    pumpCwu.state = 0;
                    break;
            };
        }

        if(param.tempAktCwu > param.tempPompaCwuStop){
             pumpCwu.state = 0;
             hisPumpCwu = 1;
        }
    }
    if(param.tempAktKotla <= param.tempAktCwu) {
        pumpCwu.state = 0;
    }
}

void TFunc::nadmuch(TParameters param)
{
    if((param.tempAktKotla > -30) && (param.tempAktKotla < 85)) {
        ERROR = NO_ERROR;
    }

    if(param.tempAktKotla <= param.tempNadmuchStart - param.hisKotla) {
        fan.state = 0;
        hisFanStart = 0;
    }

    if((param.tempAktKotla > param.tempNadmuchStart - param.hisKotla)
    && (param.tempAktKotla < param.tempNadmuchStart))
    {
        switch(hisFanStart){
            case 0 :
                fan.state = 0;
                break;
            case 1:
                fan.state = 1;

                break;
        };
    }

    if((param.tempAktKotla >=  param.tempNadmuchStart) && (param.tempAktKotla <= param.tempZadKotla - param.hisKotla)) {
        fan.state = 1;
        fan.zeroState = 0;
        hisFan = 0;
        hisFanStart = 1;
        fan.zeroTime = 0;

    }

    if((param.tempAktKotla >= param.tempNadmuchStart)
    && (param.tempAktKotla > param.tempZadKotla  - param.hisKotla)
    && (param.tempAktKotla < param.tempZadKotla))
    {
        switch(hisFan){
            case 0 :
                fan.state = 1;
                fan.zeroState = 0;
                break;
            case 1:
                rozpalanie = 0;
                toggle( param.pracaNadPod, param.przerwaNadPod, &fan.time, &fan.state, &fan.zeroTime);
                break;
        };
    }

    if((param.tempAktKotla >= param.tempZadKotla) && (param.tempAktKotla <= 80)){
         if(fan.zeroState == 0) {
            fan.state = 0;
            fan.zeroState = 1;
         }
         rozpalanie = 0;
         toggle( param.pracaNadPod, param.przerwaNadPod, &fan.time, &fan.state, &fan.zeroTime);
         hisFan = 1;
    }

    if((param.tempAktKotla > 80) && (param.tempAktKotla < 85)){
        fan.state = 0;
    }

    if(param.tempAktKotla >= 85){
        ERROR = ZA_DUZA_TEMP_KOTLA;
        fan.state = 0;
        pumpCo.state = 1;
    }
}


