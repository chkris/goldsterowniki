/*
 *      menu.cpp
 *      avrlib
 *      Created by Krzysztof Cholewka on January 2008
 *      Copyright (c). All rights reserved.
 */

/* menu is a interface for TMenu struct
 * representing informaction in program
 * its little database
 */

#ifndef _MENU_H__INCLUDED_
#define _MENU_H__INCLUDED_
/*
Raczej wszystko da się zrobić, tylko trzeba to przemyśleć i odpowiednio zakodować. Proponuje na początek poćwiczyć na prostszych typach, głównie odczyt, bo on jest tu nowością.

Co do tablicy struktur, to widzę to mniej więcej tak:

a) definicja:
- każdą strukturę definiujemy osobno, oczywiście z PROGMEM
- definiujemy tablice z elementami typu tej struktury i inicjalizujemy ją kolejnymi strukturami jako elementami, też z PROGMEM

b) odczyt (n-tej struktury z tablicy):
- tworzymy zmienną strukturalną (zwykłą, nie PROGMEM) danego typu
- kopiujemy do niej kolejne bajty zawartości z FLASH, np. jakoś tak:
Kod:
for(i=0; i<sizeof(<TYP_STRUKTURY>); i++)
  *((char*)&strukturaROM + i) = pgm_read_byte(strukturaFLASH + n * sizeof(TYP_STRUKTURY) + i)
*/

#define NO_FUNC 0

#define EEPROM_ZAD_CO       0
#define EEPROM_HIS_CO       1 // nadmuch załacza się gdy sapdnie o histereze
#define EEPROM_ZAD_CWU      2
#define EEPROM_FAN_SPEED    3
#define EEPROM_FAN_BREAK    4
#define EEPROM_FAN_WORK     5
/*
struct TMenu
{
    char*           lang[2];
    unsigned char   actualPos;
    unsigned char   ok;
    unsigned char   cancel;
    unsigned char   up;
    unsigned char   down;
    unsigned char   func; // dostępne funkcje
    unsigned char   value;//wartość zapisywana i odczytywana z eepromu
    unsigned char   min;
    unsigned char   max;
};
*/
#endif
