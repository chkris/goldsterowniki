#include "pcf8583.h"

TPcf8583::TPcf8583()
{
}

u8 TPcf8583::bcd2bin(u8 bcd)
{
#ifdef OPTIMIZE_SPEED
  return (10*(bcd>>4)|(bcd&0x0f));
#else
  u8 Temp = bcd & 0x0F;
  while (bcd >= 0x10)
  {
    Temp += 10;
    bcd -= 0x10;
  }
  return Temp;
#endif
}

u8 TPcf8583::bin2bcd(u8 bin)
{
#ifdef OPTIMIZE_SPEED
  return (((bin/10)<<4)|(bin%10));
#else
  u8 Temp = 0;
  while(bin>9)
  {
    Temp += 0x10;
    bin-=10;
  }
  return Temp+bin;
#endif
}

void TPcf8583::holdOn(void)
{
  getStatus();
  status |= 0x40;
  write(0,status);
}

void TPcf8583::holdOff(void)
{
  getStatus();
  status &= 0xbf;
  write(0,status);
}

u8 TPcf8583::getStatus(void)
{
    status = read(0);
    alarm = (status & 2);
    return status;
}

void TPcf8583::start(void)
{
  getStatus();
  status &= 0x7f;
  write(0,status);
}

void TPcf8583::stop(void)
{
    getStatus();
    status |= 0x80;
    write(0,status);
}

u8 TPcf8583::read(u8 address)
{
    u8 a;
    a = (PCF8583_A0 << 1) | 0xA0;
    i2c.start();
    i2c.write(a);
    i2c.write(address);
    i2c.start();
    i2c.write(a | 1);
    a = i2c.read(1);
    i2c.stop();
    /*
    I2C_start();
    I2C_write(a);
    I2C_write(address);
    I2C_start();
    I2C_write(a|1);
    a=I2C_read(1);
    I2C_stop();
    */
    return a;
}

void TPcf8583::write(u8 address, u8 data)
{
    i2c.start();
    i2c.write((PCF8583_A0 << 1) | 0xA0);
    i2c.write(address);
    i2c.write(data);
    i2c.stop();
/*
    I2C_start();
    I2C_write((PCF8583_A0<<1)|0xa0);
    I2C_write(address);
    I2C_write(data);
    I2C_stop();
*/
}

u8 TPcf8583::readBcd(u8 address)
{
    return bcd2bin(read(address));
}

void TPcf8583::writeBcd(u8 address,u8 data)
{
    write(address,bin2bcd(data));
}

void TPcf8583::setTime(u8 day, u8 hour,u8 min,u8 sec)
{
    stop();
    writeBcd(2,sec);
    writeBcd(3,min);
    writeBcd(4,hour);
    writeBcd(5,day);
    start();
}

void TPcf8583::getTime(u8* day, u8 *hour, u8 *min, u8 *sec)
{
    holdOn();
    *sec = readBcd(2);
    *min = readBcd(3);
    *hour = readBcd(4);
    *day = readBcd(5);
    holdOff();
}



