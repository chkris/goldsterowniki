/*
 *      ds18b20.cpp
 *      avrlib
 *      Created by Krzysztof Cholewka on January 2008
 *      Copyright (c). All rights reserved.
 */

/* function is a interface for TDs18b20 class
 * contained all function of digital sensor
 */

#ifndef _DS18B20_H__INCLUDED_
#define _DS18B20_H__INCLUDED_

#include <avr/io.h>
#include <avr/delay.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <inttypes.h>
#include <avrlib/1wire.h>

// enable DS18x20 EERPROM support
//#define DS18X20_EEPROMSUPPORT

// enable extended output via UART by defining:
//#define DS18X20_VERBOSE

/* return values */
#define DS18X20_OK          0x00
#define DS18X20_ERROR       0x01
#define DS18X20_START_FAIL  0x02
#define DS18X20_ERROR_CRC   0x03

#define DS18X20_POWER_PARASITE 0x00
#define DS18X20_POWER_EXTERN   0x01

/* DS18X20 specific values (see datasheet) */
#define DS18S20_ID 0x10
#define DS18B20_ID 0x28

#define DS18X20_CONVERT_T	0x44
#define DS18X20_READ		0xBE
#define DS18X20_WRITE		0x4E
#define DS18X20_EE_WRITE	0x48
#define DS18X20_EE_RECALL	0xB8
#define DS18X20_READ_POWER_SUPPLY 0xB4

#define DS18B20_CONF_REG    4
#define DS18B20_9_BIT       0
#define DS18B20_10_BIT      (1<<5)
#define DS18B20_11_BIT      (1<<6)
#define DS18B20_12_BIT      ((1<<6)|(1<<5))

// indefined bits in LSB if 18B20 != 12bit
#define DS18B20_9_BIT_UNDF       ((1<<0)|(1<<1)|(1<<2))
#define DS18B20_10_BIT_UNDF      ((1<<0)|(1<<1))
#define DS18B20_11_BIT_UNDF      ((1<<0))
#define DS18B20_12_BIT_UNDF      0

// conversion times in ms
#define DS18B20_TCONV_12BIT      750
#define DS18B20_TCONV_11BIT      DS18B20_TCONV_12_BIT/2
#define DS18B20_TCONV_10BIT      DS18B20_TCONV_12_BIT/4
#define DS18B20_TCONV_9BIT       DS18B20_TCONV_12_BIT/8
#define DS18S20_TCONV            DS18B20_TCONV_12_BIT

// constant to convert the fraction bits to cel*(10^-4)
#define DS18X20_FRACCONV         625

#define DS18X20_SP_SIZE  9

// DS18X20 EEPROM-Support
#define DS18X20_WRITE_SCRATCHPAD  0x4E
#define DS18X20_COPY_SCRATCHPAD   0x48
#define DS18X20_RECALL_E2         0xB8
#define DS18X20_COPYSP_DELAY      10 /* ms */
#define DS18X20_TH_REG      2
#define DS18X20_TL_REG      3


#define CRC8INIT	0x00
#define CRC8POLY	0x18              //0X18 = X^8+X^5+X^4+X^0

#define MAXSENSORS  3


typedef unsigned char u8;
typedef unsigned int  u16;

class TDs18b20
{
    public:
        TDs18b20();
        void findSensor(u8* diff, u8 id[]);
        u8 searchSensors(void);
        u8 startMeasurement(u8 withPowerExtern, u8 id[]);
        u8 readMeasurement(u8 id[], u8 *subzero, int *cel, u8 *celFracBits);
        u8 measurementToCelcius( u8 fc, u8 *sp, u8* subzero, int* cel, u8* celFracBits);
        u8 gSensorIDs[MAXSENSORS][OW_ROMCODE_SIZE];
    private:
        TOneWire oneWire;
        u8 crc8 (u8 *dataIn, u16 numberOfBytesToRead);
};


#endif
