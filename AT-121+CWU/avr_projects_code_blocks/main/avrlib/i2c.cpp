#include "i2c.h"

TI2c::TI2c()
{
}

void TI2c::start(void)
{
    I2C_SCL_H;
    I2C_SDA_H;
    I2C_SDA_WR;
    I2C_SCL_WR;
    _delay_loop_2(3);
    I2C_SDA_L;
    _delay_loop_2(3);
    I2C_SCL_L;
    _delay_loop_2(3);
}

void TI2c::stop(void)
{
    I2C_SDA_WR;		// SDA na zapis
    I2C_SCL_H;
    _delay_loop_2(3);
    I2C_SDA_H;
    _delay_loop_2(3);
}

u8 TI2c::write(u8 b)
{
    u8 i;
    I2C_SDA_WR;		// SDA na zapis
    for (i=0; i<8; i++)	// zapis 8 bitów
    {
        if (b & 0x80){ I2C_SDA_H; }
        else { I2C_SDA_L; }
        _delay_loop_2(3);
        I2C_SCL_H;
        _delay_loop_2(3);
        I2C_SCL_L;
        b <<= 1;		// przesuń o 1 bit
    }
    I2C_SDA_RD;		// włącz czytanie SDA
    I2C_SDA_H;		// podciągnij SDA
    _delay_loop_2(3);
    I2C_SCL_H;		// SCL=1
    _delay_loop_2(3);
    i=0xFF;
    do
    {
        if (bit_is_clear(I2C_PORT_I,I2C_SDA)) break;	// jeżeli jest potwierdzenie
        _delay_loop_2(3);
    }
    while(--i>0);
    I2C_SCL_L;		// SCL=0
    _delay_loop_2(3);
    return(i);
}

u8 TI2c::read(u8 ack)
{
    u8 i;
    u8 b = 0;
    I2C_SDA_RD;		// SDA na odczyt
    I2C_SDA_H;		// podciąganie SDA
    _delay_loop_2(3);
    // czytanie 8 bitów
    for (i=0; i<8; i++)
    {
        I2C_SCL_H;	// SCL w stan wysoki
        _delay_loop_2(3);
        b <<= 1;		// przesuń o 1 bit
        // jeśli SDA=1 // dodaj odczytany bit z magistrali
        if (bit_is_set(I2C_PORT_I,I2C_SDA)) { b |= 1; }
        I2C_SCL_L;	// SCL w stan niski
        _delay_loop_2(3);
    }
    I2C_SDA_WR;		// SDA na zapis
                    // ustaw bit ACK na określoną wartość
    if (ack == 0) { I2C_SDA_L; }
    else { I2C_SDA_H; }
    _delay_loop_2(3);
    I2C_SCL_H;
    _delay_loop_2(3);
    I2C_SCL_L;
    _delay_loop_2(3);
    I2C_SDA_L;
    _delay_loop_2(3);
    return(b);
}
