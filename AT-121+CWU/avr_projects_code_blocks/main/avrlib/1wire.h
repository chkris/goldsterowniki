/*
 *      iwire.cpp
 *      avrlib
 *      Created by Krzysztof Cholewka on January 2008
 *      Copyright (c). All rights reserved.
 */

/* function is a interface for Tfunction class
 * contained all function of controller
 */

#ifndef _1WIRE_H__INCLUDED_
#define _1WIRE_H__INCLUDED_

#include <avr/io.h>
#include <avr/delay.h>
#include <avr/interrupt.h>


#define OW_PIN  PD1
#define OW_IN   PIND
#define OW_OUT  PORTD
#define OW_DDR  DDRD
#define OW_CONF_DELAYOFFSET 0

#define OW_GET_IN()   ( OW_IN & (1<<OW_PIN))
#define OW_OUT_LOW()  ( OW_OUT &= (~(1 << OW_PIN)) )
#define OW_OUT_HIGH() ( OW_OUT |= (1 << OW_PIN) )
#define OW_DIR_IN()   ( OW_DDR &= (~(1 << OW_PIN )) )
#define OW_DIR_OUT()  ( OW_DDR |= (1 << OW_PIN) )

//#define OW_SHORT_CIRCUIT  0x01
#define OW_MATCH_ROM	0x55
#define OW_SKIP_ROM	    0xCC
#define	OW_SEARCH_ROM	0xF0
#define	OW_SEARCH_FIRST	0xFF		// start new search
#define	OW_PRESENCE_ERR	0xFF
#define	OW_DATA_ERR	    0xFE
#define OW_LAST_DEVICE	0x00		// last device found
//0x01 ... 0x40: continue searching
//rom-code size including CRC
#define OW_ROMCODE_SIZE 8

typedef unsigned char u8;

class TOneWire
{
    public:
        TOneWire();
        u8 inputPinState();
        void parasiteEnable(void);
        void parasiteDisable(void);
        u8 reset(void);
        u8 bitIo(u8 b);
        u8 byteWrite(u8 b);
        u8 byteRead(void);
        u8 romSearch(u8 diff, u8 *id);
        void command(u8 command, u8 *id);
};

#endif
