#include <avr/io.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
//#include <avr/signal.h>
#include <avr/wdt.h>
#include <stdlib.h>
#include <string.h>


#include "avrlib/timer1.h"
#include "avrlib/key.h"
#include "avrlib/adctemp.h"
#include "function.h"



#define DIODA_1_ON       PORTD |= _BV(2); DDRD |= _BV(2);
#define DIODA_1_OFF      PORTD &= ~_BV(2); DDRD &= ~_BV(2);
#define DIODA_1_TOG      PORTD ^= _BV(2); DDRD ^= _BV(2)

#define DIODA_2_ON       PORTD |= _BV(1); DDRD |= _BV(1)
#define DIODA_2_OFF      PORTD &= ~_BV(1); DDRD &= ~_BV(1)
#define DIODA_2_TOG      PORTD ^= _BV(1); DDRD ^= _BV(1)

#define DIODA_3_ON       PORTD |= _BV(0); DDRD |= _BV(0)
#define DIODA_3_OFF      PORTD &= ~_BV(0); DDRD &= ~_BV(0)
#define DIODA_3_TOG      PORTD ^= _BV(0); DDRD ^= _BV(0)

#define DIODA_4_ON       PORTD |= _BV(7); DDRD |= _BV(7)
#define DIODA_4_OFF      PORTD &= ~_BV(7); DDRD &= ~_BV(7)
#define DIODA_4_TOG      PORTD ^= _BV(7); DDRD ^= _BV(7)

#define DIODA_5_ON       PORTC |= _BV(0); DDRC |= _BV(0)
#define DIODA_5_OFF      PORTC &= ~_BV(0); DDRC &= ~_BV(0)
#define DIODA_5_TOG      PORTC ^= _BV(0); DDRC ^= _BV(0)

#define DIODA_1_M_ON       PORTB |= _BV(0); DDRB |= _BV(0)
#define DIODA_1_M_OFF      PORTB &= ~_BV(0); DDRB &= ~_BV(0)
#define DIODA_1_M_TOG      PORTB ^= _BV(0); DDRB ^= _BV(0)

#define DIODA_2_M_ON       PORTB |= _BV(1); DDRB |= _BV(1)
#define DIODA_2_M_OFF      PORTB &= ~_BV(1); DDRB &= ~_BV(1)
#define DIODA_2_M_TOG      PORTB ^= _BV(1); DDRB ^= _BV(1)

#define DIODA_3_M_ON       PORTB |= _BV(3); DDRB |= _BV(3)
#define DIODA_3_M_OFF      PORTB &= ~_BV(3); DDRB &= ~_BV(3)
#define DIODA_3_M_TOG      PORTB ^= _BV(3); DDRB ^= _BV(3)

#define DIODA_4_M_ON       PORTB |= _BV(4); DDRB |= _BV(4)
#define DIODA_4_M_OFF      PORTB &= ~_BV(4); DDRB &= ~_BV(4)
#define DIODA_4_M_TOG      PORTB ^= _BV(4); DDRB ^= _BV(4)

#define DIODA_1_T_ON       PORTB |= _BV(6); DDRB |= _BV(6)//TO DZIWNIE DZIALA ?
#define DIODA_1_T_OFF      PORTB &= ~_BV(6); DDRB &= ~_BV(6)
#define DIODA_1_T_TOG      PORTB ^= _BV(6); DDRB ^= _BV(6)

#define DIODA_2_T_ON       PORTB |= _BV(7); DDRB |= _BV(7)
#define DIODA_2_T_OFF      PORTB &= ~_BV(7); DDRB &= ~_BV(7)
#define DIODA_2_T_TOG      PORTB ^= _BV(7); DDRB ^= _BV(7)



#define W_T_D_ON       PORTC &= ~_BV(5); DDRC |= _BV(5);
#define W_T_D_OFF      PORTC |= _BV(5); DDRC &= ~_BV(5);


#define W_T_J_ON       PORTC &= ~_BV(4); DDRC |= _BV(4);
#define W_T_J_OFF      PORTC |= _BV(4); DDRC &= ~_BV(4);


#define W_C_ON       PORTC &= ~_BV(3); DDRC |= _BV(3);
#define W_C_OFF      PORTC &= ~_BV(3); DDRC &= ~_BV(3);
#define W_C_TOG      PORTC ^= _BV(3); DDRC ^= _BV(3);

#define W_D_ON       PORTC &= ~_BV(2); DDRC |= _BV(2);
#define W_D_OFF      PORTC &= ~_BV(2); DDRC &= ~_BV(2);
#define W_D_TOG      PORTC ^= _BV(2); DDRC ^= _BV(2);

#define W_E_ON       PORTC &= ~_BV(1); DDRC |= _BV(1);
#define W_E_OFF      PORTC &= ~_BV(1); DDRC &= ~_BV(1);
#define W_E_TOG      PORTC ^= _BV(1); DDRC ^= _BV(1);

#define W_A_ON       PORTC &= ~_BV(7); DDRC |= _BV(7);
#define W_A_OFF      PORTC &= ~_BV(7); DDRC &= ~_BV(7);
#define W_A_TOG      PORTC ^= _BV(7); DDRC ^= _BV(7);

#define W_B_ON       PORTC &= ~_BV(6); DDRC |= _BV(6);
#define W_B_OFF      PORTC &= ~_BV(6); DDRC &= ~_BV(6);
#define W_B_TOG      PORTC ^= _BV(6); DDRC ^= _BV(6);

#define W_F_ON       PORTA &= ~_BV(6); DDRA |= _BV(6);
#define W_F_OFF      PORTA &= ~_BV(6); DDRA &= ~_BV(6);
#define W_F_TOG      PORTA ^= _BV(6); DDRA ^= _BV(6);

#define W_G_ON       PORTA &= ~_BV(7); DDRA |= _BV(7);
#define W_G_OFF      PORTA &= ~_BV(7); DDRA &= ~_BV(7);
#define W_G_TOG      PORTA ^= _BV(7); DDRA ^= _BV(7);

/**THE FACTIC WORK OF CONTROLLER SHOULD BE DEVINE FOR NORMAL WORK AND ERRORS**/
#define MAKE_BYTE(b0,b1,b2,b3,b4,b5,b6,b7) (b0*(1 << 0) + b1*(1 << 1) + b2*(1 << 2) + b3*(1 << 3) + b4*(1 << 4) + b5*(1 << 5) + b6*(1 << 6) + b7*(1 << 7))


#define MRUGAJ                  20
#define MAX_OBROTY_WENTYLATORA  8
#define MAX_WAIT_SWITCH         500
#define WAIT                    70

#define TEMP_SCREEN         0
#define TEMP_SET            1
#define ROZPALANIE          2
#define PRZEDMUCH_PRZERWA   3
#define PRZEDMUCH_PRACA     4
#define POMPA_CO_START      5
#define NADMUCH_START       6
#define HISTEREZA_KOTLA     7
#define POMPA_CWU_START     8
#define OBROTY_WENTYLATORA  9
#define TEMP_CWU_SCREEN     10
#define TEMP_CWU_SET        11

TFunc       func;
TKey        key;
TAdcTemp    adcTemp;
unsigned char keyDelay = 0;
volatile unsigned char change = 0;
volatile unsigned int delayScreen = 0;
unsigned char changeDiode = 0;
volatile unsigned int wait = 0;
volatile unsigned int wait_5s = 0;
volatile unsigned char mruganie = 50;
volatile unsigned char menuDiode = 0;
volatile u8 hzCounter;
volatile u8 fineFan = 0;
volatile u8 tmpHzCounter = 0;
volatile unsigned char diodMruganie = 0;
volatile unsigned char nrDiody = 0;
volatile unsigned char diodeMruganie = 0;

volatile unsigned char E1change = 0; // zmienna odpowiadaj�ca za zmian� wy�wietlanych informacji przy b��dzie E1

unsigned char eePompaCwuStart        __attribute__((section(".eeprom"))) = 40;
unsigned char eePompaCwuStop         __attribute__((section(".eeprom"))) = 50;
unsigned char eeTempSet             __attribute__((section(".eeprom"))) = 55;
unsigned char eePrzedmuchPrzerwa    __attribute__((section(".eeprom"))) = 10;
unsigned char eePrzedmuchPraca      __attribute__((section(".eeprom"))) = 10;
unsigned char eePompaCoStart        __attribute__((section(".eeprom"))) = 40;
unsigned char eeNadmuchStart        __attribute__((section(".eeprom"))) = 35;
unsigned char eeHisterezaKotla      __attribute__((section(".eeprom"))) = 2;
unsigned char eeHisterezaPompaCo    __attribute__((section(".eeprom"))) = 2;
unsigned char eeObrotyWentylatora   __attribute__((section(".eeprom"))) = MAX_OBROTY_WENTYLATORA;


//ZNAK(A,B,C,D,E,F,G,NIEUZYWANY BIT)

unsigned char znak[13] = {
    MAKE_BYTE(1,1,1,1,1,1,0,0),//0
    MAKE_BYTE(0,1,1,0,0,0,0,0),//1
    MAKE_BYTE(1,1,0,1,1,0,1,0),//2
    MAKE_BYTE(1,1,1,1,0,0,1,0),//3
    MAKE_BYTE(0,1,1,0,0,1,1,0),//4
    MAKE_BYTE(1,0,1,1,0,1,1,0),//5
    MAKE_BYTE(1,0,1,1,1,1,1,0),//6
    MAKE_BYTE(1,1,1,0,0,0,0,0),//7
    MAKE_BYTE(1,1,1,1,1,1,1,0),//8
    MAKE_BYTE(1,1,1,1,0,1,1,0),//9
    MAKE_BYTE(1,0,0,1,1,1,1,0),//E
    MAKE_BYTE(0,1,1,1,1,1,0,0),//U
    MAKE_BYTE(1,1,0,0,1,1,1,0),//R
};

unsigned char dioda[9] = {
    MAKE_BYTE(0,0,0,0,0,0,0,0),//0
    MAKE_BYTE(1,0,0,0,0,0,0,0),//1
    MAKE_BYTE(0,1,0,0,0,0,0,0),//2
    MAKE_BYTE(0,0,1,0,0,0,0,0),//3
    MAKE_BYTE(0,0,0,1,0,0,0,0),//4
    MAKE_BYTE(0,0,0,0,1,0,0,0),//5
    MAKE_BYTE(0,0,0,0,0,1,0,0),//6
    MAKE_BYTE(0,0,0,0,0,0,1,0),//7
    MAKE_BYTE(0,0,0,0,0,0,0,1),//8
};



struct TMenu {
    unsigned char min;
    unsigned char max;
};

struct TActMenu {
    unsigned char toCwu;
    int toCwuCount;
    unsigned char index;
    unsigned char lastIndex;
    unsigned int value;
    unsigned char state;
} actMenu;

struct TScreen {
   volatile  unsigned int mrug;
   volatile  unsigned int show;
} screen;


static struct TMenu menu[] __attribute__ ((progmem)) = {
    {0 ,0 }, //0 - TEMP_SCREEN
    {45,80}, //1 - TEMP_SET
    {0 ,0 }, //2 - ROZPALANIE
    {1 ,99}, //3 - PRZEDMUCH_PRZERWA
    {1 ,99}, //4 - PRZEDMUCH_PRACA
    {10,50}, //5 - POMPA_CO_START
    {20,50}, //6 - NADMUCH_START
    {1 ,10}, //7 - HISTEREZA_KOTLA
    {10 ,80}, //8 - POMPA_CWU_STAERT
    {1 ,MAX_OBROTY_WENTYLATORA}, //9 - OBROTY_WENTYLATORA
    {0 ,0 }, //10 - TEMP_CWU_SCREEN
    {30,80}, //1 - TEMP_CWU_SET
};








void dataTurnOnDiode(
        unsigned char d
)
{
    DIODA_1_M_OFF;
    DIODA_2_M_OFF;
    DIODA_3_M_OFF;
    DIODA_4_M_OFF;

    DIODA_2_T_OFF;
    DIODA_1_T_OFF;

    switch(changeDiode){
        case 0:
            changeDiode = 1;
            if(dioda[d] & (1 << 0)){DIODA_1_M_ON;}
            if(dioda[d] & (1 << 2)){DIODA_2_M_ON;}
            if(dioda[d] & (1 << 4)){DIODA_3_M_ON;}
            if(dioda[d] & (1 << 6)){DIODA_4_M_ON;}
            DIODA_2_T_ON;
        break;
        case 1:
            changeDiode = 0;
            if(dioda[d] & (1 << 1)){DIODA_1_M_ON;}
            if(dioda[d] & (1 << 3)){DIODA_2_M_ON;}
            if(dioda[d] & (1 << 5)){DIODA_3_M_ON;}
            if(dioda[d] & (1 << 7)){DIODA_4_M_ON;}
            DIODA_1_T_ON;
        break;
    }



}


void DiodeOn(
        unsigned char d,
        int mruganie
)
{
//
    if(mruganie > 1) {
         if(diodMruganie < mruganie) {
            diodMruganie++;
            dataTurnOnDiode(d);
        }
        if(diodMruganie < 3 * mruganie && diodMruganie >=  mruganie) {
            diodMruganie++;
            dataTurnOnDiode(0);
        }

        if(diodMruganie == 2 * mruganie) {diodMruganie = 0;}
    }
    if(mruganie == 1) {
        dataTurnOnDiode(d);
    }
    if(mruganie == 0) {
        dataTurnOnDiode(0);
    }
}


void dataOnScreen(
        unsigned char data
)
{
    W_A_OFF;
    W_B_OFF;
    W_C_OFF;
    W_D_OFF;
    W_E_OFF;
    W_F_OFF;
    W_G_OFF;

    if(znak[data] & (1 << 0)) {W_A_ON;}
    if(znak[data] & (1 << 1)) {W_B_ON;}
    if(znak[data] & (1 << 2)) {W_C_ON;}
    if(znak[data] & (1 << 3)) {W_D_ON;}
    if(znak[data] & (1 << 4)) {W_E_ON;}
    if(znak[data] & (1 << 5)) {W_F_ON;}
    if(znak[data] & (1 << 6)) {W_G_ON;}
}


void writeOnScreen(
        unsigned int data,
        unsigned int mruganie
)
{
    W_T_D_OFF;W_T_J_OFF;
    switch(change){
        case 0:
            if(data >= 100) {
                dataOnScreen(data / 100);
            } else {
                dataOnScreen(data / 10);
            }
            W_T_D_ON;
            break;
        case 1:
            if(data >= 100) {
                dataOnScreen(data % 100);
            } else {
                dataOnScreen(data % 10);
            }

            W_T_J_ON;
            break;
    }

    if(mruganie == 0) {change = (change + 1) % 2;}
    if(mruganie > 0) {
        if(delayScreen < mruganie) {change = 2;}
        if(delayScreen == mruganie) {change = 0;}
        if(delayScreen > mruganie) {change = (change + 1) % 2;}
        if(delayScreen < 2 * mruganie) {delayScreen++;}
        if(delayScreen == 2 * mruganie) {delayScreen = 0;}
    }



}


void oneMenu(
        uint8_t* adress,
        unsigned char keys,
        unsigned char minMenu,
        unsigned char maxMenu,
        unsigned char nextMenu,
        unsigned char prevMenu,
        unsigned char aMenu,
        unsigned int mrug,
        unsigned char mDiode
)
{
//

    switch(actMenu.state) {
        case 0:
            actMenu.value = eeprom_read_byte(adress);
            nrDiody = mDiode;
            diodeMruganie = 20;
            screen.mrug = 0;
            if(keys == KEY_DOWN) {
                wait_5s = 0;
                actMenu.index = nextMenu;
                key.lastKey = 0;
            }
            if(keys == KEY_UP) {
                wait_5s = 0;
                actMenu.index = prevMenu;
                key.lastKey = 0;
            }
            if(keys == KEY_OPTION) {
                wait = WAIT;
                wait_5s = 0;
                actMenu.state = 1;
                key.lastKey = 0;
            }
        break;
        case 1:
            nrDiody = mDiode;
            diodeMruganie = 1;
            if(keys == KEY_UP) {
                wait_5s = 0;
                if(actMenu.value < maxMenu) {
                    screen.mrug = 0;
                    wait = 0;
                    actMenu.value++;
                }
                key.lastKey = 0;
            }

            if(keys == KEY_DOWN) {
            wait_5s = 0;
            if(minMenu < actMenu.value) {
                screen.mrug = 0;
                wait = 0;
                actMenu.value--;
            }
            key.lastKey = 0;
        }

            if(keys == KEY_OPTION) {
                wait_5s = 0;
                if(eeprom_read_byte(adress) != actMenu.value) {
                    eeprom_write_byte(adress,actMenu.value);
                }
                actMenu.state = 0;
                key.lastKey = 0;
            }
        break;
    }




        if(wait == WAIT) {
            screen.mrug = mrug;
            wait = WAIT + 1;
        }
        if(wait_5s == MAX_WAIT_SWITCH) {
            switch(actMenu.lastIndex) {
                case TEMP_SCREEN: actMenu.index = TEMP_SCREEN;
                break;
                case TEMP_CWU_SCREEN: actMenu.index = TEMP_CWU_SCREEN;
                break;
            }
            wait_5s = MAX_WAIT_SWITCH + 1;
            screen.mrug = 0;
            diodeMruganie = 0;
            actMenu.state = 0;
        }
}


void changeMenu(
        unsigned char keys
)
{

    switch(actMenu.index) {


        case TEMP_CWU_SET:

            if(wait == WAIT) {
                screen.mrug = MRUGAJ;
                wait = WAIT + 1;
            }
            if(keys == KEY_UP) {
                if(actMenu.value < pgm_read_byte(&menu[TEMP_CWU_SET].max) ) {
                    screen.mrug = 0;
                    wait = 0;
                    actMenu.value++;
                }
                key.lastKey = 0;
            }
            if(keys == KEY_DOWN) {
                if(pgm_read_byte(&menu[TEMP_CWU_SET].min) < actMenu.value) {
                    screen.mrug = 0;
                    wait = 0;
                    actMenu.value--;
                }
                key.lastKey = 0;
            }

            if(keys == KEY_OPTION) {
                if(eeprom_read_byte(&eePompaCwuStop) != actMenu.value) {
                    eeprom_write_byte(&eePompaCwuStop,actMenu.value);
                }
                actMenu.index = TEMP_CWU_SCREEN;
                screen.mrug = 0;
                key.lastKey = 0;
            }

        break;

        case TEMP_SET:

            if(wait == WAIT) {
                screen.mrug = MRUGAJ;
                wait = WAIT + 1;
            }
            if(keys == KEY_UP) {
                if(actMenu.value < pgm_read_byte(&menu[TEMP_SET].max)) {
                    screen.mrug = 0;
                    wait = 0;
                    actMenu.value++;
                }
                key.lastKey = 0;
            }
            if(keys == KEY_DOWN) {
                if(pgm_read_byte(&menu[TEMP_SET].min) < actMenu.value) {
                    screen.mrug = 0;
                    wait = 0;
                    actMenu.value--;
                }
                key.lastKey = 0;
            }

            if(keys == KEY_OPTION) {
                if(eeprom_read_byte(&eeTempSet) != actMenu.value) {
                    eeprom_write_byte(&eeTempSet,actMenu.value);
                }
                actMenu.index = TEMP_SCREEN;
                screen.mrug = 0;
                key.lastKey = 0;
            }




        break;


        case TEMP_CWU_SCREEN:
            DIODA_5_ON;
            DIODA_4_OFF;
            actMenu.value = adcTemp.tempCwu;
            if(adcTemp.tempCwu < 0) {
                actMenu.value = 0;
            }
            if(adcTemp.tempCwu > 99) {
                actMenu.value = 99;
            }

            screen.mrug = 0;



            if(key.keyOff == KEY_OPTION && actMenu.toCwu == TEMP_CWU_SCREEN) {
                actMenu.state = 0;
                wait_5s = 0;
                actMenu.index = ROZPALANIE;
                key.lastKey = 0;
                key.keyOff = 0;
            }
            if(key.keyPressed == KEY_OPTION && key.countKey > DELAY_KEEP - 5) {
                actMenu.index = TEMP_SCREEN;
                key.countKey = 0;
            }

            if(keys == KEY_UP || keys == KEY_DOWN) {
                actMenu.index = TEMP_CWU_SET;
                actMenu.value = eeprom_read_byte(&eePompaCwuStop);
                wait_5s = 0;
                screen.mrug = 20;
                key.lastKey = 0;
            }
            if(actMenu.toCwu == TEMP_SCREEN){
                actMenu.toCwuCount++;
                if(actMenu.toCwuCount > 1000){
                    actMenu.toCwuCount = 0;
                    actMenu.toCwu = TEMP_CWU_SCREEN;
                    key.keyOff = 0;
                }
            }
            actMenu.lastIndex = TEMP_CWU_SCREEN;

        break;

        /////////////////////////////////////////////////////////////////////////////

        case TEMP_SCREEN:
            DIODA_4_ON;
            DIODA_5_OFF;

            actMenu.value = adcTemp.tempCo;
            if(adcTemp.tempCo < 0) {
                actMenu.value = 0;
            }
            if(adcTemp.tempCo > 99) {
                actMenu.value = 99;
            }
            screen.mrug = 0;



            if(key.keyOff == KEY_OPTION && actMenu.toCwu == TEMP_SCREEN) {
                actMenu.state = 0;
                wait_5s = 0;
                actMenu.index = ROZPALANIE;
                key.lastKey = 0;
                key.keyOff = 0;
            }
            if(key.keyPressed == KEY_OPTION && key.countKey > DELAY_KEEP - 5) {
                actMenu.index = TEMP_CWU_SCREEN;

                key.countKey = 0;
            }

            if(keys == KEY_UP || keys == KEY_DOWN) {
                actMenu.index = TEMP_SET;
                actMenu.value = eeprom_read_byte(&eeTempSet);
                wait_5s = 0;
                screen.mrug = 20;
                key.lastKey = 0;
            }

            if(actMenu.toCwu == TEMP_CWU_SCREEN){
                actMenu.toCwuCount++;
                if(actMenu.toCwuCount > 1000){
                    actMenu.toCwuCount = 0;
                    actMenu.toCwu = TEMP_SCREEN;
                    key.keyOff = 0;
                }
            }

            actMenu.lastIndex = TEMP_SCREEN;

        break;

        //////////////////////////////////////////////////////////////////////

        case ROZPALANIE :

            switch(actMenu.state) {
            case 0:
                nrDiody = 8;
                diodeMruganie = 20;
                actMenu.value = 1210;
                screen.mrug = 0;
            break;
            case 1:
                if(func.rozpalanie == 0){
                    func.rozpalanie = 1;
                } else {
                    func.rozpalanie = 0;
                }
                diodeMruganie = 1;
                screen.mrug = 20;
                actMenu.state = 2;
            break;
            case 2://tutj nic nie robi
            break;
        }

        if(wait_5s == MAX_WAIT_SWITCH) {
            switch(actMenu.lastIndex) {
                case TEMP_SCREEN: actMenu.index = TEMP_SCREEN;
                break;
                case TEMP_CWU_SCREEN: actMenu.index = TEMP_CWU_SCREEN;
                break;
            }
            wait_5s = MAX_WAIT_SWITCH + 1;
            screen.mrug = 0;
            nrDiody = 0;
            diodeMruganie = 0;
            actMenu.state = 0;
        }

        if(keys == KEY_DOWN && actMenu.state == 0) {
            actMenu.index = PRZEDMUCH_PRZERWA;
            key.lastKey = 0;
        }

        if(keys == KEY_OPTION) {
            if(actMenu.state == 0 ) {
                actMenu.state = 1;
            } else {
                actMenu.state = 0;
            }
            wait_5s = 0;
            key.lastKey = 0;
        }

        if(keys == KEY_UP && actMenu.state == 0) {
            actMenu.index = OBROTY_WENTYLATORA;
            key.lastKey = 0;
        }
/*
        if(keys == KEY_CANCEL) {
            actMenu.index = TEMP_SCREEN;
            wait_5s = MAX_WAIT_SWITCH + 1;
            screen.mrug = 0;
            nrDiody = 0;
            wait = WAIT + 1;
            key.lastKey = 0;
        }
*/
        break;

        ////////////////////////////////////////////////////////

        case PRZEDMUCH_PRZERWA:
            oneMenu(&eePrzedmuchPrzerwa ,keys,pgm_read_byte(&menu[PRZEDMUCH_PRZERWA].min)    ,
                    pgm_read_byte(&menu[PRZEDMUCH_PRZERWA].max)
                    ,PRZEDMUCH_PRACA ,ROZPALANIE, PRZEDMUCH_PRZERWA
                    ,MRUGAJ,7);
        break;

        case PRZEDMUCH_PRACA:
            oneMenu(&eePrzedmuchPraca   ,keys,pgm_read_byte(&menu[PRZEDMUCH_PRACA].min)      ,
                    pgm_read_byte(&menu[PRZEDMUCH_PRACA].max)
                    ,POMPA_CO_START , PRZEDMUCH_PRZERWA, PRZEDMUCH_PRACA
                    ,MRUGAJ,6);
        break;

        case POMPA_CO_START:
            oneMenu(&eePompaCoStart     ,keys,pgm_read_byte(&menu[POMPA_CO_START].min)       ,
                    pgm_read_byte(&menu[POMPA_CO_START].max)
                    ,NADMUCH_START , PRZEDMUCH_PRACA, POMPA_CO_START
                    ,MRUGAJ,5);

        break;

        case NADMUCH_START:
            oneMenu(&eeNadmuchStart     ,keys,pgm_read_byte(&menu[NADMUCH_START].min)        ,
                    pgm_read_byte(&menu[NADMUCH_START].max)
                    ,HISTEREZA_KOTLA , POMPA_CO_START , NADMUCH_START
                    ,MRUGAJ,4);

        break;

        case HISTEREZA_KOTLA:
            oneMenu(&eeHisterezaKotla   ,keys,pgm_read_byte(&menu[HISTEREZA_KOTLA].min)      ,
                    pgm_read_byte(&menu[HISTEREZA_KOTLA].max)
                    ,POMPA_CWU_START , NADMUCH_START , HISTEREZA_KOTLA
                    ,MRUGAJ,3);
        break;

        case POMPA_CWU_START :
            oneMenu(&eePompaCwuStart,keys,pgm_read_byte(&menu[POMPA_CWU_START ].min)   ,
                    pgm_read_byte(&menu[POMPA_CWU_START ].max)
                    ,OBROTY_WENTYLATORA , HISTEREZA_KOTLA , POMPA_CWU_START
                    ,MRUGAJ,2);
        break;

         case OBROTY_WENTYLATORA:
             oneMenu(&eeObrotyWentylatora,keys,pgm_read_byte(&menu[OBROTY_WENTYLATORA].min)   ,
                    pgm_read_byte(&menu[OBROTY_WENTYLATORA].max)
                    ,ROZPALANIE , POMPA_CWU_START , OBROTY_WENTYLATORA
                    ,MRUGAJ,1);
        break;


    };

    screen.show = actMenu.value;
}


void test()
{
BUZER_ON;
DIODA_1_ON;
DIODA_2_ON;
DIODA_3_ON;
DIODA_4_ON;
DIODA_5_ON;


DIODA_2_T_ON;
DIODA_1_T_ON;
DIODA_1_M_ON;
DIODA_2_M_ON;
DIODA_3_M_ON;
DIODA_4_M_ON;

W_T_J_ON;
W_T_D_ON;
W_C_ON;
W_D_ON;
W_E_ON;
W_A_ON;
W_B_ON;
W_F_ON;
W_G_ON;

for(int i = 0; i < 30; i++)
{
_delay_loop_2(50000);
}
DIODA_1_OFF;
DIODA_2_OFF;
DIODA_3_OFF;
DIODA_4_OFF;
DIODA_5_OFF;
BUZER_OFF;
W_T_J_OFF;
W_T_D_OFF;

DIODA_2_T_OFF;
DIODA_1_T_OFF;

for(int i = 0; i < 10; i++)
{
_delay_loop_2(50000);
}

}


int main(void)
{
    test();
    adcTemp.init();
    key.init();
    actMenu.index = TEMP_SCREEN;
    actMenu.state = 0;
    wait_5s = MAX_WAIT_SWITCH  + 1;

    TCCR0 = INTP0_CLK1024;               //prescaler 64
    TCNT0 = 0;                           //reset counter
    TIMSK |= _BV(TOIE0);                 // enable TCNT0 overflow interrupt

    //TCCR2 = INTP0_CLK64;               //prescaler 64
    //TCNT2 = 216;                       //reset counter
    //TIMSK |= _BV(TOIE2);               // enable TCNT0 overflow interrupt

    TCCR1B = INTP0_CLK8;
    TCNT1H = 0;
    TCNT1L = 0;
    TIMSK |= (1 << TOIE1);


    GICR |= (1 << INT2);
    // set INT0 to edge detection
    MCUCR |= (1 << ISC2);
    // timer overflow interrupt enable
    TIMSK |= _BV(TOIE2);

    wdt_disable();
    sei();

    adcTemp.tempCo = (int)adcTemp.temp(10,0).temp;       // temp CO
    adcTemp.wynikCo = adcTemp.temp(10,0).wynik;                          // ADC CO - warto�� z przetwornika

    adcTemp.tempCwu = (int)adcTemp.temp(10,1).temp; // temp Cwu
    adcTemp.wynikCwu = adcTemp.temp(10,1).wynik;                        // ADC Cwu - warto�� z przetwornika

    while(1){



        if(func.rozpalanie == 1) {
            func.param.tempNadmuchStart = 0;
        }
        if(func.rozpalanie == 0) {
            func.param.tempNadmuchStart = eeprom_read_byte(&eeNadmuchStart);
        }

        func.param.tempZadKotla         = eeprom_read_byte(&eeTempSet);
        func.param.tempPumpCoStart      = eeprom_read_byte(&eePompaCoStart);
        func.param.hisKotla             = eeprom_read_byte(&eeHisterezaKotla );
        //func.param.hisPompaCo         = eeprom_read_byte(&eeHisterezaPompaCo );
        func.param.przerwaNadPod        = eeprom_read_byte(&eePrzedmuchPrzerwa);
        func.param.pracaNadPod          = eeprom_read_byte(&eePrzedmuchPraca );
        func.param.tempAktKotla         = adcTemp.tempCo;
        func.param.tempPompaCwuStart    = eeprom_read_byte(&eePompaCwuStart);
        func.param.tempPompaCwuStop     = eeprom_read_byte(&eePompaCwuStop);
        func.param.tempAktCwu           = adcTemp.tempCwu;

        func.nadmuch(func.param);
        func.pompaCo(func.param);
        func.pompaCwu(func.param);

        //mierzenie temperatury
        if(adcTemp.buferTime == BUFER_TIME) {

            adcTemp.buferCo = adcTemp.buferCo + (int)adcTemp.temp(10,0).temp;       // temp CO
            adcTemp.wynikCo = adcTemp.temp(10,0).wynik;                          // ADC CO - warto�� z przetwornika

            adcTemp.buferCwu = adcTemp.buferCwu + (int)adcTemp.temp(10,1).temp; // temp Cwu
            adcTemp.wynikCwu = adcTemp.temp(10,1).wynik;                        // ADC Cwu - warto�� z przetwornika

            adcTemp.buferCount++;
            adcTemp.buferTime = 0;
            if(adcTemp.buferCount == BUFER_COUNT) {

                adcTemp.tempCo = (int)(adcTemp.buferCo / BUFER_COUNT);
                adcTemp.tempCwu = (int)(adcTemp.buferCwu / BUFER_COUNT);

                adcTemp.buferCount = 0;
                adcTemp.buferCo  = 0;
                adcTemp.buferCwu = 0;
            }
        }
        if(adcTemp.wynikCo < 390 || adcTemp.wynikCo > 700 ){
               func.ERROR = BLAD_CZUJNIKA_CO;
        }
        if(adcTemp.wynikCwu < 390 || adcTemp.wynikCwu > 700 ){
               func.ERROR = BLAD_CZUJNIKA_CWU;
        }
        //koniec pomiaru temeratury

        switch(func.ERROR) {
            case NO_ERROR :
                changeMenu(key.lastKey);
                switch(actMenu.lastIndex) {
                    case TEMP_SCREEN: DIODA_4_ON;DIODA_5_OFF;
                    break;
                    case TEMP_CWU_SCREEN:DIODA_4_ON;DIODA_5_OFF;
                    break;
                };
            break;
            case ZA_DUZA_TEMP_KOTLA :
                // mruganie na przemian temperatura i b��d E1
                if(E1change >= 0 && E1change < 50) {
                    BUZER_ON;
                    screen.show = adcTemp.tempCo;
                    if(adcTemp.tempCo < 0){screen.show = 0;}
                    if(adcTemp.tempCo > 99){screen.show = 99;}
                }
                if(E1change >= 50 && E1change < 100) {
                    BUZER_ON;
                    screen.show = 1001;
                }
                if(E1change > 100) {
                    E1change = 0;
                }
                //**************************************************//
                func.pumpCo.state = 1;
                func.fan.state = 0;
                key.lastKey = 0;
            break;
            case BLAD_CZUJNIKA_CO :
                screen.show = 1002;
                BUZER_ON;
                func.pumpCo.state = 1;
                func.pumpCwu.state = 1;
                func.fan.state = 0;
                key.lastKey = 0;
            break;
            case BLAD_CZUJNIKA_CWU :
                screen.show = 1003;
                BUZER_ON;
                func.pumpCo.state = 1;
                func.pumpCwu.state = 1;
                func.fan.state = 0;
                key.lastKey = 0;
            break;

        }
        // jak ustawia kto� temperature to wy�acza nadmuch, mo�na w tym czasie do pieca przy�o�y�
        if(actMenu.index == TEMP_SET) {func.fan.state = 0;}

        switch(func.fan.state) {
            case 0:
                MOC_OFF;
                _delay_loop_2(5);
                DIODA_2_OFF;FAN_OFF;
            break;
            case 1:
                DIODA_2_ON;FAN_ON;
            break;
        };

        switch(func.pumpCo.state) {
            case 0: DIODA_1_OFF;PUMP_CO_OFF;
            break;
            case 1: DIODA_1_ON;PUMP_CO_ON;
            break;
        }
        switch(func.pumpCwu.state) {
            case 0: DIODA_3_OFF; PUMP_CWU_ON;
            break;
            case 1: DIODA_3_ON;PUMP_CWU_OFF;
            break;
        }



       fineFan = eeprom_read_byte(&eeObrotyWentylatora);
       if(fineFan == MAX_OBROTY_WENTYLATORA) {
            MOC_ON;
       }

    }
    return 0;
}


SIGNAL(SIG_INTERRUPT2)
{
cli();

    hzCounter = 0;
    TCNT1H = 248 - (MAX_OBROTY_WENTYLATORA - fineFan);
    TCNT1L = 240;
    //niech odczeka min 0,54ms
sei();
}


SIGNAL(SIG_OVERFLOW1)
{
    if(fineFan != MAX_OBROTY_WENTYLATORA){
            MOC_ON;
            _delay_loop_2(3);
            MOC_OFF;
            TCNT1H = 0;
            TCNT1L = 0;
    }
}


ISR(SIG_OVERFLOW0)
{
    TCNT0 = 216;
    if(wait < WAIT){
        wait++;
    }

    if(adcTemp.buferTime < BUFER_TIME){
        adcTemp.buferTime++;
    }

    if(wait_5s < MAX_WAIT_SWITCH) {
        wait_5s++;
    }
    key.test();

    func.mesureTime(&func.fan.time);
    func.mesureTime(&func.pumpCo.time);
    writeOnScreen(screen.show,screen.mrug);
    DiodeOn(nrDiody,diodeMruganie);
    E1change++;

}
