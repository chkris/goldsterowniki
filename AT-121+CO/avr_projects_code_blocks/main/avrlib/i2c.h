/*
 *      i2c.cpp
 *      avrlib
 *      Created by Krzysztof Cholewka on February 2008
 *      Copyright (c). All rights reserved.
 */

/* i2c is a interface for TI2c class
 * contained all function of i2c protocol
 */

#ifndef _I2C_H__INCLUDED_
#define _I2C_H__INCLUDED_

#include <avr/io.h>
#include <avr/delay.h>

typedef unsigned char u8;

#define I2C_SDA		1
#define I2C_SCL 	0

#define I2C_PORT_O	    PORTC
#define	I2C_PORT_D	    DDRC
#define I2C_PORT_I	    PINC

#define I2C_SDA_WR  	(I2C_PORT_D |= (1 << I2C_SDA))
#define I2C_SDA_RD	    (I2C_PORT_D &= ~(1 << I2C_SDA))
#define I2C_SCL_WR	    (I2C_PORT_D |= (1 << I2C_SCL))
#define I2C_SCL_RD 	    (I2C_PORT_D &= ~(1 << I2C_SCL))

#define I2C_SDA_H       (I2C_PORT_O |= (1 << I2C_SDA))
#define I2C_SDA_L       (I2C_PORT_O &= ~(1 << I2C_SDA))
#define I2C_SCL_H       (I2C_PORT_O |= (1 << I2C_SCL))
#define I2C_SCL_L       (I2C_PORT_O &= ~(1 << I2C_SCL))


class TI2c {

    public:
        TI2c();
        u8 write(u8 b);
        u8 read(u8 ack);
        void start(void);
        void stop(void);
};

#endif
