#include "ds18b20.h"

TDs18b20::TDs18b20()
{
}

void TDs18b20::findSensor(u8* diff, u8 id[])
{
    for (;;) {
        *diff = oneWire.romSearch(*diff, &id[0]);
        if ( *diff==OW_PRESENCE_ERR || *diff==OW_DATA_ERR ||
            *diff == OW_LAST_DEVICE ) return;
        if ( id[0] == DS18B20_ID || id[0] == DS18S20_ID ) return;
    }
}

u8 TDs18b20::startMeasurement(u8 withPowerExtern, u8 id[])
{
    oneWire.reset();
	if(oneWire.inputPinState()) { // only send if bus is "idle" = high
		oneWire.command(DS18X20_CONVERT_T, id);
		if (withPowerExtern != DS18X20_POWER_EXTERN)
			oneWire.parasiteEnable();
		return DS18X20_OK;
	}
	else {
		#ifdef DS18X20_VERBOSE
		#endif
		return DS18X20_START_FAIL;
	}
}


u8 TDs18b20::readMeasurement(u8 id[], u8 *subzero, int *cel, u8 *celFracBits)
{
	u8 i;
	u8 sp[DS18X20_SP_SIZE];

	oneWire.reset();
	oneWire.command(DS18X20_READ, id);
	for ( i=0 ; i< DS18X20_SP_SIZE; i++ ) sp[i] = oneWire.byteRead();
	if ( crc8( &sp[0], DS18X20_SP_SIZE ) )
		return DS18X20_ERROR_CRC;
	measurementToCelcius(id[0], sp, subzero, cel, celFracBits);
	return DS18X20_OK;
}

u8 TDs18b20::measurementToCelcius( u8 fc, u8 *sp, u8* subzero, int* cel, u8* celFracBits)
{
	unsigned int meas;
	unsigned char  i;

	meas = sp[0];  // LSB
	meas |= ((unsigned int)sp[1])<<8; // MSB
	//meas = 0xff5e; meas = 0xfe6f;

	//  only work on 12bit-base
	if( fc == DS18S20_ID ) { // 9 -> 12 bit if 18S20
		/* Extended measurements for DS18S20 contributed by Carsten Foss */
		meas &= (unsigned int) 0xfffe;	// Discard LSB , needed for later extended precicion calc
		meas <<= 3;					// Convert to 12-bit , now degrees are in 1/16 degrees units
		meas += (16 - sp[6]) - 4;	// Add the compensation , and remember to subtract 0.25 degree (4/16)
	}

	// check for negative
	if ( meas & 0x8000 )  {
		*subzero=1;      // mark negative
		meas ^= 0xffff;  // convert to positive => (twos complement)++
		meas++;
	}
	else *subzero=0;

	// clear undefined bits for B != 12bit
	if ( fc == DS18B20_ID ) { // check resolution 18B20
		i = sp[DS18B20_CONF_REG];
		if ( (i & DS18B20_12_BIT) == DS18B20_12_BIT ) ;
		else if ( (i & DS18B20_11_BIT) == DS18B20_11_BIT )
			meas &= ~(DS18B20_11_BIT_UNDF);
		else if ( (i & DS18B20_10_BIT) == DS18B20_10_BIT )
			meas &= ~(DS18B20_10_BIT_UNDF);
		else { // if ( (i & DS18B20_9_BIT) == DS18B20_9_BIT ) {
			meas &= ~(DS18B20_9_BIT_UNDF);
		}
	}

	*cel  = (unsigned int)(meas >> 4);
	*celFracBits = (unsigned int)((meas & 0x000F) % 10);

	return DS18X20_OK;
}


u8	TDs18b20::crc8 (u8 *dataIn, u16 numberOfBytesToRead)
{
	u8  crc;
	u16 loopCount;
	u8  bitCounter;
	u8  data;
	u8  feedbackBit;

	crc = CRC8INIT;

	for (loopCount = 0; loopCount != numberOfBytesToRead; loopCount++)
	{
		data = dataIn[loopCount];

		bitCounter = 8;
		do {
			feedbackBit = (crc ^ data) & 0x01;

			if ( feedbackBit == 0x01 ) {
				crc = crc ^ CRC8POLY;
			}
			crc = (crc >> 1) & 0x7F;
			if ( feedbackBit == 0x01 ) {
				crc = crc | 0x80;
			}

			data = data >> 1;
			bitCounter--;

		} while (bitCounter > 0);
	}
	return crc;
}

u8 TDs18b20::searchSensors(void)
{
    u8 i;
	u8 id[OW_ROMCODE_SIZE];
	u8 diff, nSensors;

	for(diff = OW_SEARCH_FIRST;diff != OW_LAST_DEVICE && nSensors < MAXSENSORS ; ) {
		findSensor( &diff, &id[0] );
		if(diff == OW_PRESENCE_ERR) { break; }
		if(diff == OW_DATA_ERR) { break;}
		for (i=0;i < OW_ROMCODE_SIZE;i++) { gSensorIDs[nSensors][i]=id[i]; }
		nSensors++;
	}
	return nSensors;
}
