#include "key.h"

TKey::TKey()
{
}
void TKey::init(void)
{
    PORTD |= _BV(3) + _BV(4) + _BV(5) + _BV(6);
	DDRD |= ~(_BV(3) + _BV(4) + _BV(5) + _BV(6));



	keyPressed = KEY_NO_PRESS;
	countKey = 0;
	delayKey = 0;
}

void TKey::test(void)
{
    keyPressed = KEY_NO_PRESS;
    if(TEST_SWITCH(1)) { keyPressed = KEY_UP;}
    if(TEST_SWITCH(4)) { keyPressed = KEY_DOWN;}
   // if(TEST_SWITCH(4)) { keyPressed = KEY_CANCEL;}
    if(TEST_SWITCH(2)) { keyPressed = KEY_OPTION;}

    if(keyPressed > 0){

        if(countKey > DELAY_KEEP) {
            switch(delayKey){
                case 1: lastKey = keyPressed;break;
                case DELAY_TIME: delayKey = 0;break;
            }
            delayKey++;
        }

        if(countKey == 0) {
            lastKey = keyPressed;
            BUZER_ON;
            buzerOff = 0;
        }
        countKey++;
    }

    if(keyPressed == 0) {
        countKey = 0;
        lastKey = 0;
    }
}
