#include "adctemp.h"
#include <math.h>

TAdcTemp::TAdcTemp()
{

}
void TAdcTemp::init(void)
{
	DDRA &= ~(_BV(PA0) + _BV(PA1));//+ _BV(PA2) + _BV(PA3));// + _BV(PA4) + _BV(PA5) + _BV(PA6) + _BV(PA7));		    // PORT's as an input
	PORTA &= ~(_BV(PA0) + _BV(PA1));// + _BV(PA2) + _BV(PA3));// + _BV(PA4) + _BV(PA5) + _BV(PA6) + _BV(PA7));		    // Setting up the PORT's
                                //Wybranie wewn�trznego �r�d�a odniesienia
	ADMUX |= _BV(REFS0);
	ADMUX &= ~_BV(REFS1);
                                //Wybranie sposobu zapisu wyniku z wyr�wnaniem do lewej (osiem starszych bit�w wyniku w rejestrze ADCH)
	ADMUX |= _BV(ADLAR);
                                //Zezwolenie na konwersje
	ADCSRA |= _BV(ADEN);
                                //Wybranie cz�stotliwo�ci dla taktowania przetwornika  (1/8 cz�stotliwosci zegara kontrolera)
	ADCSRA |= _BV(ADPS0);
	ADCSRA |= _BV(ADPS1);
	//ADCSRA |= _BV(ADPS2);

	bufer = 0;
	buferTime = 0;
	buferCount = 0;
}


 TTempError TAdcTemp::temp(int precision, unsigned char canal)
{
    unsigned int    wynik;
    unsigned char   counter;
    float           R2;
    float           deltaTA;
    float           T1;
    counter     =   precision;
    wynik       =   0;
    TTempError  temperatureWithWynik =  {0,0};

    ADMUX &= ~(_BV(MUX0) + _BV(MUX1) + _BV(MUX2) + _BV(MUX3) + _BV(MUX4));


    ADMUX |= canal;

    while(counter){
        ADCSRA |= _BV(ADSC);                     // Rozpocz�cie przetwarzania
        while(bit_is_set(ADCSRA,ADSC)){};		 // Oczekiwanie na zako�czenie przetwarzania
        wynik = wynik + ADCW /(1 << 6);			 // Sumowanie wynik�w pomiar�w dokonywanych w serii
        counter--;
    }
    wynik = wynik / precision;                   //liczenie �redniej
    //od 390 do 700 w�a�ciwy zakres, poza zakresem b��d E2

    R2 = wynik * A / (5 - wynik * DU_10_BIT);
    deltaTA = sqrt(B - C * (RT25 - R2));
    T1 = 25 + (-D + deltaTA) / E;

    temperatureWithWynik.temp = T1;
    temperatureWithWynik.wynik = wynik;

    return temperatureWithWynik;

}


/*
TMyFloat TAdcTemp::dTTemp(int precision, unsigned char canal)
 {
    unsigned int    wynik = 0;
    unsigned char   counter = precision;
    TMyFloat        temp = {0,0};
    ADMUX &= ~(_BV(MUX0) + _BV(MUX1) + _BV(MUX2) + _BV(MUX3) + _BV(MUX4));


    ADMUX |= canal;

    while(counter){
        ADCSRA |= _BV(ADSC);                     // Rozpocz�cie przetwarzania
        while(bit_is_set(ADCSRA,ADSC)){};		 // Oczekiwanie na zako�czenie przetwarzania
        wynik = wynik + ADCW /(1 << 6);			 // Sumowanie wynik�w pomiar�w dokonywanych w serii
        counter--;
    }

    wynik = wynik / precision;                   //liczenie �redniej
    wynik = (wynik - 459);                       //460 jednostek oznacza 0 C

    temp.all = (wynik * 54) / 100;             // dT na jednostk� 0.544 C
    temp.decimal = ((wynik * 54) - temp.all * 10) % 10;

    temperatureWithWynik.temp = T1;
    temperatureWithWynik.wynik = wynik;

    return temperatureWithWynik;
}
*/
