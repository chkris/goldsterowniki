/*
 *      key.cpp
 *      avrlib
 *      Created by Krzysztof Cholewka on January 2008
 *      Copyright (c). All rights reserved.
 */

/* key is a interface for TKey class
 * contained all helpfull function for
 * detecting press, keep switches.
 */

#ifndef _KEY_H__INCLUDED_
#define _KEY_H__INCLUDED_

#include <avr/io.h>

#define SWITCH_PIN_1 PIND
#define SWITCH_1 PIND3

#define SWITCH_PIN_2 PIND
#define SWITCH_2 PIND4

#define SWITCH_PIN_3 PIND
#define SWITCH_3 PIND5

#define SWITCH_PIN_4 PIND
#define SWITCH_4 PIND6

#define TEST_SWITCH(NR) !(SWITCH_PIN_## NR & _BV(SWITCH_## NR))

#define KEY_NO_PRESS    0
#define KEY_UP          1
#define KEY_DOWN        2
#define KEY_OPTION      3
#define KEY_CANCEL      4

#define DELAY_KEEP 70
#define DELAY_TIME 6

#define BUZER_ON DDRB |= (1 << PB5); PORTB |= (1 << PB5)
#define BUZER_OFF DDRB &= ~(1 << PB5); PORTB |= (1 << PB5)

class TKey
{
    public:
        TKey();
        void init(void);
        void test(void);
        volatile char keyPressed;
        volatile char lastKey;
        volatile int countKey;
        volatile char buzerOff;
        volatile unsigned char delayKey;
};

#endif
