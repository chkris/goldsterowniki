/*
 *      timer1.cpp
 *      avrlib
 *      Created by Krzysztof Cholewka on January 2008
 *      Copyright (c). All rights reserved.
 */

/* timer1
 */

#ifndef _TIMER1_H__INCLUDED_
#define _TIMER1_H__INCLUDED_

#define INTP0_NO        0
#define INTP0_CLK1      _BV(CS00)
#define INTP0_CLK8      _BV(CS01)
#define INTP0_CLK64     _BV(CS01) + _BV(CS00)
#define INTP0_CLK256    _BV(CS02)
#define INTP0_CLK1024   _BV(CS00) + _BV(CS02)

class TTimerOne
{
    public:
        TTimerOne();
        void init(void);
};

#endif
