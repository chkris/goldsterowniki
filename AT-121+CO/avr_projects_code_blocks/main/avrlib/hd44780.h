/*
 *      hd44780.cpp
 *      avrlib
 *      Created by Krzysztof Cholewka on January 2008
 *      Copyright (c). All rights reserved.
 */

/* hd44780 is a interface for TLcd class
 * contained all helpfull function for
 * driving lcd screen.
 */

#ifndef _HD44780_H__INCLUDED_
#define _HD44780_H__INCLUDED_

#include <avr/io.h>
#include <avr/delay.h>
#include <avr/pgmspace.h>

//that means 1000000
#define MY_F_CPU 1

#define LCD_RS_DIR		DDRB
#define LCD_RS_PORT 	PORTB
#define LCD_RS_PIN		PINB
#define LCD_RS			(1 << PB0)

#define LCD_RW_DIR		DDRB
#define LCD_RW_PORT		PORTB
#define LCD_RW_PIN		PINB
#define LCD_RW			(1 << PB1)

#define LCD_E_DIR		DDRB
#define LCD_E_PORT		PORTB
#define LCD_E_PIN		PINB
#define LCD_E			(1 << PB3)

#define LCD_DB4_DIR		DDRB
#define LCD_DB4_PORT	PORTB
#define LCD_DB4_PIN		PINB
#define LCD_DB4			(1 << PB4)

#define LCD_DB5_DIR		DDRB
#define LCD_DB5_PORT	PORTB
#define LCD_DB5_PIN		PINB
#define LCD_DB5			(1 << PB5)

#define LCD_DB6_DIR		DDRB
#define LCD_DB6_PORT	PORTB
#define LCD_DB6_PIN		PINB
#define LCD_DB6			(1 << PB6)

#define LCD_DB7_DIR		DDRB
#define LCD_DB7_PORT	PORTB
#define LCD_DB7_PIN		PINB
#define LCD_DB7			(1 << PB7)

//-------------------------------------------------------------------------------------------------
//
// Instrukcje kontrolera Hitachi HD44780
//
//-------------------------------------------------------------------------------------------------

#define HD44780_CLEAR					0x01

#define HD44780_HOME					0x02

#define HD44780_ENTRY_MODE				0x04
	#define HD44780_EM_SHIFT_CURSOR		0
	#define HD44780_EM_SHIFT_DISPLAY	1
	#define HD44780_EM_DECREMENT		0
	#define HD44780_EM_INCREMENT		2

#define HD44780_DISPLAY_ONOFF			0x08
	#define HD44780_DISPLAY_OFF			0
	#define HD44780_DISPLAY_ON			4
	#define HD44780_CURSOR_OFF			0
	#define HD44780_CURSOR_ON			2
	#define HD44780_CURSOR_NOBLINK		0
	#define HD44780_CURSOR_BLINK		1

#define HD44780_DISPLAY_CURSOR_SHIFT	0x10
	#define HD44780_SHIFT_CURSOR		0
	#define HD44780_SHIFT_DISPLAY		8
	#define HD44780_SHIFT_LEFT			0
	#define HD44780_SHIFT_RIGHT			4

#define HD44780_FUNCTION_SET			0x20
	#define HD44780_FONT5x7				0
	#define HD44780_FONT5x10			4
	#define HD44780_ONE_LINE			0
	#define HD44780_TWO_LINE			8
	#define HD44780_4_BIT				0
	#define HD44780_8_BIT				16
#define HD44780_CGRAM_SET				0x40
#define HD44780_DDRAM_SET				0x80

#define MAKE_BYTE(b0,b1,b2,b3,b4,b5,b6,b7) (b0*(1 << 0) + b1*(1 << 1) + b2*(1 << 2) + b3*(1 << 3) + b4*(1 << 4) + b5*(1 << 5) + b6*(1 << 6) + b7*(1 << 7))
#define MAKE_LCD_BYTE(b0,b1,b2,b3,b4) (b4*(1 << 0) + b3*(1 << 1) + b2*(1 << 2) + b1*(1 << 3) + b0*(1 << 4))


#define X 1

class TLcd
{
    public:
        char* tx;
        TLcd();
        void outNibble(unsigned char);
        unsigned char inNibble(void);
        void write(unsigned char dataToWrite);
        unsigned char read(void);
        void init(void);
        void writeCommand(unsigned char);
        unsigned char readStatus(void);
        void writeData(unsigned char);
        unsigned char readData(void);
        void writeText(char *);
        void goTo(unsigned char, unsigned char);
        void clear(void);
        void home(void);
        void writeNumber(int allOutNumber);
        void writeFlash(char* p);
        void defCustomChar(char *s);
        void defCel(void);


};

#endif
