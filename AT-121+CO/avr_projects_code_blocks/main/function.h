/*
 *      function.cpp
 *      avrlib
 *      Created by Krzysztof Cholewka on January 2008
 *      Copyright (c). All rights reserved.
 */

/* function is a interface for Tfunction class
 * contained all function of controller
 */

#ifndef _FUNCTION_H__INCLUDED_
#define _FUNCTION_H__INCLUDED_

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>



#define FAN_ON          PORTA |= _BV(3); DDRA |= _BV(3);
#define FAN_OFF         PORTA &= ~_BV(3); DDRA &= ~_BV(3);


#define PUMP_CO_ON      PORTA |= _BV(4); DDRA |= _BV(4);
#define PUMP_CO_OFF     PORTA &= ~_BV(4); DDRA &= ~_BV(4);


#define PUMP_CWU_ON     PORTA |= _BV(5); DDRB |= _BV(5);
#define PUMP_CWU_OFF    PORTA &= ~_BV(5); DDRB &= ~_BV(5);

#define MOC_ON          PORTA |= _BV(2); DDRA |= _BV(2)
#define MOC_OFF         PORTA &= ~_BV(2); DDRA &= ~_BV(2)

#define MAX_TEMP_CO     80


typedef unsigned char u8;

#define NO_ERROR            0
#define BLAD_CZUJNIKA_CO    1
#define ZA_DUZA_TEMP_KOTLA  2

#define ANTY_STOP_PRZERWA 5
#define ANTY_STOP_PRACA 30



struct TTime
{
    unsigned int ms;
    unsigned int s;
    unsigned int m;
    unsigned int h;
    unsigned int d;
};

struct TParameters {
    unsigned char tempZadKotla;
    int tempAktKotla;
    unsigned char tempPumpCoStart;
    unsigned char tempNadmuchStart;
    unsigned char hisKotla;
    unsigned char hisPompaCo;
    unsigned char przerwaNadPod;
    unsigned char pracaNadPod;
};


struct TFan {
    char zeroTime;
    char state;
    struct TTime time;
    char zeroState;
};

struct TPumpCo {
    char zeroTime;
    char state;
    struct TTime time;
};

class TFunc
{
    public:
        TFunc();
        unsigned char rozpalanie;
        unsigned char hisFanStart;  //przy załączaniu nadmuchu histereza, taka sama jak przy osiągniętej temperaturze
        unsigned char hisFan;
        unsigned char hisPumpCo;
        unsigned char ERROR;
        TFan fan;
        TPumpCo pumpCo;
        TTime fanTime, pumpCoTime;
        TParameters param;

        void mesureTime(struct TTime* time);
        void setTime(u8 d,u8 h, u8 m, u8 s, u8 ms, struct TTime* time);
        void toggle(unsigned int work, unsigned int pause,struct TTime* time, char* state, char* zeroTime);
        void nadmuch(TParameters param);
        void pompaCo(TParameters param);
};

#endif
