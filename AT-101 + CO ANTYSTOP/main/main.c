/*Projekt oparty o ATTiny13-20PU
 */

#include <avr/io.h>

#define PRZEK_ON          PORTB |= _BV(PB1); DDRB |= _BV(PB1)
#define PRZEK_OFF         PORTB &= ~_BV(PB1); DDRB &= ~_BV(PB1)

//#define BUZER_ON          DDRB |= _BV(PB1); PORTB |= _BV(PB1)
//#define BUZER_OFF         DDRB &= ~_BV(PB1); PORTB |= _BV(PB1)


void initTemp(void)
{
	DDRB &= ~(_BV(PB0) + _BV(PB2) + _BV(PB3));// + _BV(PA4) + _BV(PA5) + _BV(PA6) + _BV(PA7));		    // PORT's as an input
	PORTB &= ~(_BV(PB0) + _BV(PB2) + _BV(PB3));// + _BV(PA4) + _BV(PA5) + _BV(PA6) + _BV(PA7));		    // Setting up the PORT's
                                //Wybranie wewn�trznego �r�d�a odniesienia
	ADMUX &= ~(1 << REFS0);
                           //Wybranie sposobu zapisu wyniku z wyr�wnaniem do lewej (osiem starszych bit�w wyniku w rejestrze ADCH)
	ADMUX |= _BV(ADLAR);
                                //Zezwolenie na konwersje
	ADCSRA |= _BV(ADEN);
                                //Wybranie cz�stotliwo�ci dla taktowania przetwornika  (1/8 cz�stotliwosci zegara kontrolera)
	ADCSRA |= _BV(ADPS0);
	ADCSRA |= _BV(ADPS1);
	//ADCSRA |= _BV(ADPS2);
}


unsigned int Temp(int precision, unsigned char canal, char x)
 {
    unsigned int    wynik = 0;
    unsigned char   counter = precision;
    ADMUX &= ~(_BV(MUX0) + _BV(MUX1));
    unsigned int r = 0;


    ADMUX |= canal;

    while(counter){
        ADCSRA |= _BV(ADSC);                     // Rozpocz�cie przetwarzania
        while(bit_is_set(ADCSRA,ADSC)){};		 // Oczekiwanie na zako�czenie przetwarzania
        wynik = wynik + ADCW/(1 << 6);			 // Sumowanie wynik�w pomiar�w dokonywanych w serii
        counter--;
    }

    wynik = wynik / precision;                   //liczenie �redniej

    if(x == 0) {
            //460 jednostek oznacza 0 C
    //r = (wynik * 54) / 100;
        wynik = (wynik - 459);
        r = wynik * 5;
        r = r * 10;
        r = r / 100;
        if(r >= 58 && r <= 65){r = r + 1;}
        if(r >= 66 && r <= 71){r = r + 1;}
        //if(r >= 72 && r <= 76){r = r + 1;}
        if(r >= 74 && r <= 78){r = r + 1;}
        if(r >  78 && r <= 85){r = r + 2;}
        if(r >= 86 && r <= 88){r = r + 3;}
        if(r >= 89 && r <= 91){r = r + 5;}
        if(r >= 92){r = r + 7;}
    }

    if(x == 1) {
        r= wynik;
        r = wynik/17;
        r = r + 25;
        if(r >= 35 && r < 45) {r = r - 1;}
        if(r >= 45 && r < 65) {r = r - 2;}
        if(r >= 65 && r <= 80) {r = r - 3;}
        if(r >  80) {r = r - 4;}
    }

    return r;
}

struct Time {
int ms;
char s;
char m;
char h;
char d;
};

int main(void)
{
    int tempPot = 0;
    int tempAkt = 0;
    char hys = 0;
    struct Time time = {0,0,0,0,0};



    initTemp();

    while(1){
    if(time.ms < 999) {
        time.ms = time.ms + 2;
    } else {
        time.ms = 0;
        if(++time.s > 59) {
            time.s = 0;
            if(++time.m > 59) {
                time.m = 0;
                if(++time.h > 24) {
                    time.h = 0;
                }
            }
        }
    }

        tempPot = Temp(5,3,1);
        tempAkt = Temp(5,1,0);

        if(tempAkt < tempPot - 3) { hys = 0; }
        if(tempAkt >= tempPot) { hys = 1; }

        if(time.d >= 5 && time.m <= 11) {
            hys = 1;
            if(time.m >= 10) {
                time.d = 0;
                time.h = 0;
                time.m = 0;
                time.s = 0;
            }
        }

        switch(hys) {
            case 0: PRZEK_OFF;break;
            case 1: PRZEK_ON;break;
        }





    }
    return 0;
}
